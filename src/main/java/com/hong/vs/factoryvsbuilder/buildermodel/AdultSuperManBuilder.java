package com.hong.vs.factoryvsbuilder.buildermodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:38
 * @Description:
 */
public class AdultSuperManBuilder extends Builder {

    @Override
    public SuperMan getSuperMan() {
        super.setBody("强壮的躯体");
        super.setSpecialTalent("会飞行");
        super.setSpecialSymbol("胸前带S标记");
        return super.superMan;
    }
}
