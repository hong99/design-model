package com.hong.vs.factoryvsbuilder.buildermodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:41
 * @Description:未成年超人建造者
 */
public class ChildSuperManBuilder extends Builder {

    @Override
    public SuperMan getSuperMan() {
        super.setBody("强壮的躯体");
        super.setSpecialTalent("刀枪不入");
        super.setSpecialSymbol("胸前带小S标记");
        return super.superMan;
    }
}
