package com.hong.vs.factoryvsbuilder.buildermodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:45
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        //建造一个成年超人
        SuperMan adultSuperMan = Director.getAdultSuperMan();
        //展示一下超人的信息
        adultSuperMan.getSpecialTalent();
    }
}
