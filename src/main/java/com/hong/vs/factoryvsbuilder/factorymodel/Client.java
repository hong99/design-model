package com.hong.vs.factoryvsbuilder.factorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:20
 * @Description:场景类
 */
public class Client {
    public static void main(String[] args) {
        //生产一个成年超人
        ISuperMan adultSuperMan = SuperManFactory.createSuperMan("adult");
        //展示一下超人的技能
        adultSuperMan.specialTalent();
    }
}
