package com.hong.vs.factoryvsbuilder.factorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:18
 * @Description:未成年超人
 */
public class ChildSuperMan implements ISuperMan {
    @Override
    public void specialTalent() {
        System.out.println("小超人的能力刀枪不入、快速运动");
    }
}
