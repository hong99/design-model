package com.hong.vs.factoryvsbuilder.factorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:14
 * @Description:超人接口
 */
public interface ISuperMan {
    //每个超人都有特殊技能
    public void specialTalent();
}
