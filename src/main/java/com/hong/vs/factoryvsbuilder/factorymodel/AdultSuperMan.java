package com.hong.vs.factoryvsbuilder.factorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 10:17
 * @Description:成年超人
 */
public class AdultSuperMan implements ISuperMan {
    @Override
    public void specialTalent() {
        System.out.println("超人力大无穷");
    }
}
