package com.hong.base.other_type_model.business_delegate_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 17:09
 * @Description: 客服端(client)
 */
public class Client {

    BusinessDelegate businessDelegate;

    public Client(BusinessDelegate businessDelegate) {
        this.businessDelegate = businessDelegate;
    }

    public void doTask(){
        businessDelegate.doTask();
    }
}
