package com.hong.base.other_type_model.business_delegate_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 17:10
 * @Description:由具体业务代表来调用业务
 */
public class Test {
    public static void main(String[] args) {
        BusinessDelegate businessDelegate = new BusinessDelegate();
        businessDelegate.setServiceType("EJB");

        Client client = new Client(businessDelegate);
        client.doTask();

        businessDelegate.setServiceType("JMS");
        client.doTask();
    }
}
