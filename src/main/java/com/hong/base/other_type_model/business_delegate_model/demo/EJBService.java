package com.hong.base.other_type_model.business_delegate_model.demo;

/**
 *
 * 功能描述:Business Service
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/23 18:30
 */
public class EJBService implements BusinessService {
 
   @Override
   public void doProcessing() {
      System.out.println("Processing task by invoking EJB Service");
   }
}