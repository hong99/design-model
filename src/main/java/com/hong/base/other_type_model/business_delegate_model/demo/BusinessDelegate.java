package com.hong.base.other_type_model.business_delegate_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 17:08
 * @Description: 业务代表（Business Delegate）
 */
public class BusinessDelegate {
    private BusinessLookUp lookUp = new BusinessLookUp();
    private BusinessService businessService;
    private String serviceType;

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void doTask(){
        businessService = lookUp.getBusinessService(serviceType);
        businessService.doProcessing();
    }
}
