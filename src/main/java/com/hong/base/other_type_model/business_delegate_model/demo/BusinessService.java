package com.hong.base.other_type_model.business_delegate_model.demo;
/**
 *
 * 功能描述:抽象业务接口
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/23 18:30
 */
public interface BusinessService {
   public void doProcessing();
}