package com.hong.base.other_type_model.business_delegate_model.demo;
/**
 *
 * 功能描述:查询服务(LookUp Service)
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/23 18:30
 */
public class BusinessLookUp {
   public BusinessService getBusinessService(String serviceType){
      if(serviceType.equalsIgnoreCase("EJB")){
         return new EJBService();
      }else {
         return new JMSService();
      }
   }
}