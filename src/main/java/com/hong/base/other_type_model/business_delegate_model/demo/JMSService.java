package com.hong.base.other_type_model.business_delegate_model.demo;
/**
 *
 * 功能描述:业务接口(Business Service)
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/23 18:30
 */
public class JMSService implements BusinessService {
 
   @Override
   public void doProcessing() {
      System.out.println("Processing task by invoking JMS Service");
   }
}