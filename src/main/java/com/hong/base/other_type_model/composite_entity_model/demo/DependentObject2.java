package com.hong.base.other_type_model.composite_entity_model.demo;

/**
 * @Auther: Administrator
 * @Date: 2020-06-26 11:31
 * @Description:依赖对象
 */
public class DependentObject2 {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    
}
