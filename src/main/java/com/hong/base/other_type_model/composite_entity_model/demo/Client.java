package com.hong.base.other_type_model.composite_entity_model.demo;

/**
 * @Auther: csh
 * @Date: 2020-06-26 11:49
 * @Description:测试组合实体对象
 */
public class Client {
    private CompositeEntity compositeEntity = new CompositeEntity();

    public void pintData(){
        for (String s : compositeEntity.getData()) {
            System.out.println("Data:"+s);
        }
    }

    public void setData(String data1,String data2){
        compositeEntity.setData(data1,data2);
    }
}
