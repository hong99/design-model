package com.hong.base.other_type_model.composite_entity_model.demo;

/**
 * @Auther: csh
 * @Date: 2020-06-26 11:46
 * @Description:组合体
 */
public class CompositeEntity {
    private CoarseGrainedObject cgo = new CoarseGrainedObject();
    public void setData(String data1,String data2){
        cgo.setData(data1,data2);
    }

    public String[] getData(){
        return  cgo.getData();
    }
}
