package com.hong.base.other_type_model.composite_entity_model.demo;

/**
 * @Auther: csh
 * @Date: 2020-06-26 12:04
 * @Description:组合实体模式
 */
public class Test {

    public static void main(String[] args) {
        Client client = new Client();
        client.setData("Test","Data");
        client.pintData();
        client.setData("test2","data2");
        client.pintData();
    }
}
