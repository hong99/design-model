package com.hong.base.other_type_model.transfer_object_model.demo;

import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/5 16:15
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        UserBo userBo = new UserBo();
        for (UserVo userVo : userBo.getUserVos()) {
            System.out.println(userVo.toString());
        }
        //更新
        UserVo user = userBo.getUser(0);
        System.out.println(user.toString());
        user.setUserName("user2");
        userBo.updateUserInfo(0,user);
        //重新打印
        System.out.println(userBo.getUser(0).toString());

    }
}
