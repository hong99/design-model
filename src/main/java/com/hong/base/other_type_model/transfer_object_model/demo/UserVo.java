package com.hong.base.other_type_model.transfer_object_model.demo;

import java.io.Serializable;

/**
 * @Auther: csh
 * @Date: 2020/7/5 16:01
 * @Description:uesrVo
 */
public class UserVo implements Serializable {
    private String consumerId;
    private String userName;

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserVo(String consumerId, String userName) {
        this.consumerId = consumerId;
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "consumerId='" + consumerId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
