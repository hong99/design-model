package com.hong.base.other_type_model.transfer_object_model.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/5 16:02
 * @Description:用户bo
 */
public class UserBo implements Serializable {
    List<UserVo> userVos;

    public UserBo() {
        userVos = new ArrayList <UserVo>();
        userVos.add(new UserVo("1","用户1"));
        userVos.add(new UserVo("2","用户2"));
    }

    public void delete(UserVo userVo){
        userVos.remove(userVo.getConsumerId());
        System.out.println("删除的用户信息"+userVo.toString());
    }

    //获取所有用户
    public List<UserVo> getUserVos(){
        return userVos;
    }

    public UserVo getUser(int index){
        return userVos.get(index);
    }

    public void updateUserInfo(Integer index,UserVo userVo){
        userVos.get(index).setUserName(userVo.getUserName());
        System.out.println("更新的用户是:"+userVo.toString());
    }
}
