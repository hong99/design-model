package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:43
 * @Description:测试
 */
public class Client {
    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher();
        GradeController gradeController = new GradeController(dispatcher);
        gradeController.dispatcherRequest(1);
        gradeController.dispatcherRequest(10);
        gradeController.dispatcherRequest(100);
        gradeController.dispatcherRequest(80);
    }
}
