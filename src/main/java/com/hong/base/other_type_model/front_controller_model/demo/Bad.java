package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:12
 * @Description:成绩不及格（view）
 */
public class Bad extends Grade {

    private static Bad bad = new Bad();

    private void Bad(){

    }


    public static Bad getInstance(){
        return bad;
    }

    @Override
    public void show() {
        System.out.println("成绩不及格");
    }
}
