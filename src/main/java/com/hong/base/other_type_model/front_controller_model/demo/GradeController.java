package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:40
 * @Description:控制器
 */
public class GradeController {
    private Dispatcher dispatcher;

    public GradeController(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    private void log(int grade){
        System.out.println("请求的成绩是:"+grade);
    }

    private Boolean isAuthenticUser(){
        System.out.println("默认通过");
        return true;
    }

    public void dispatcherRequest(int grade){
        //打印日志
        log(grade);
        //对用户进行鉴权
        if(isAuthenticUser()){
            dispatcher.dispatch(grade);
        }
    }
}
