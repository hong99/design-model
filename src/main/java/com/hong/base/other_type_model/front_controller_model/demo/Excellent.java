package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:10
 * @Description:view
 */
public class Excellent extends Grade {


    public static Excellent  excellent = new Excellent();


    private  Excellent(){

    }

    public static Excellent getInstance(){
        return excellent;
    }

    @Override
    public void show() {
        System.out.println("成绩优秀。");
    }
}
