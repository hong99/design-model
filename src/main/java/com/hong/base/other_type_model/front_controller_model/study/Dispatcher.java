package com.hong.base.other_type_model.front_controller_model.study;
/**
 *
 * 功能描述:调度器
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/29 10:04
 */
public class Dispatcher {
   private StudentView studentView;
   private HomeView homeView;
   public Dispatcher(){
      studentView = new StudentView();
      homeView = new HomeView();
   }
 
   public void dispatch(String request){
      if(request.equalsIgnoreCase("STUDENT")){
         studentView.show();
      }else{
         homeView.show();
      }  
   }
}