package com.hong.base.other_type_model.front_controller_model.study;
/**
 *
 * 功能描述:视图
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/29 10:04
 */
public class HomeView {
   public void show(){
      System.out.println("Displaying Home Page");
   }
}