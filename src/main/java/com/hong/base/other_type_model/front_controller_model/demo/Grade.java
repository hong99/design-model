package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:09
 * @Description:抽象的视图
 */
public abstract class Grade {
    public abstract void show();
}
