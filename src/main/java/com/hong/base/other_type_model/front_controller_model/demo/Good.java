package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:11
 * @Description:成绩良 view
 */
public class Good extends Grade {

    private static Good good = new Good();

    private Good(){

    }

    public static Good getInstance(){
        return good;
    }



    @Override
    public void show() {
        System.out.println("成绩良好");
    }
}
