package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:12
 * @Description:调度器
 */
public class Dispatcher {

    public void dispatch(int grade){
        if(grade<60){
            Bad.getInstance().show();
        }else if(grade>=60 && grade<70){
            General.getInstance().show();
        }else if(grade>=70 && grade<80){
            Good.getInstance().show();
        }else{

            Excellent.getInstance().show();
        }
    }


}
