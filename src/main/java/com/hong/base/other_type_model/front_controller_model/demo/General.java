package com.hong.base.other_type_model.front_controller_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/29 10:11
 * @Description:
 */
public class General extends Grade {

    private static General general = new General();

    @Override
    public void show() {
        System.out.println("成绩一般");
    }


    private General(){

    }


    public static General getInstance(){
        return general;
    }
}
