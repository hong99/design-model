package com.hong.base.other_type_model.front_controller_model.study;

/**
 * @Auther: csh
 * @Date: 2020/6/29 09:32
 * @Description:前端控制器模式测试
 */
public class Client {
    public static void main(String[] args) {
        FrontController frontController = new FrontController();
        frontController.dispatchRequest("HOME");
        frontController.dispatchRequest("STUDENT");
    }
}
