package com.hong.base.other_type_model.data_access_object_model.demo;

import java.util.List;
 /**
  *
  * 功能描述:接口
  *
  * @param:
  * @return:
  * @auther: csh
  * @date: 2020/6/28 10:29
  */
public interface StudentDao {
    /**
     *
     * 功能描述: 全部学生
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/6/28 10:29
     */
   public List<Student> getAllStudents();
   /**
    *
    * 功能描述:通过学生号获取
    *
    * @param:
    * @return:
    * @auther: csh
    * @date: 2020/6/28 10:29
    */
   public Student getStudent(int rollNo);
   /**
    *
    * 功能描述:更新学生信息
    *
    * @param:
    * @return:
    * @auther: csh
    * @date: 2020/6/28 10:30
    */
   public void updateStudent(Student student);
   /**
    *
    * 功能描述:删除学生信息
    *
    * @param:
    * @return:
    * @auther: csh
    * @date: 2020/6/28 10:30
    */
   public void deleteStudent(Student student);
}