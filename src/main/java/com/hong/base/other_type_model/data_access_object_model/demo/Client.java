package com.hong.base.other_type_model.data_access_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/28 10:31
 * @Description: 通过
 */
public class Client {
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDaoImpl();

        //输出所有的学生
        for (Student allStudent : studentDao.getAllStudents()) {
            System.out.println("学生【学号:"+allStudent.getRollNo()+"姓名:"+allStudent.getName()+"】");
        }

        //更新学生信息
        Student student = studentDao.getAllStudents().get(0);
        student.setName("hong");
        studentDao.updateStudent(student);

        //获取学生信息
        Student now = studentDao.getAllStudents().get(0);
        System.out.println("姓名:"+now.getName()+"学号:"+now.getRollNo());
    }
}
