package com.hong.base.other_type_model.data_access_object_model.demo;
/**
 *
 * 功能描述:学生
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/28 10:29
 */
public class Student {
   private String name;
   private int rollNo;
 
   Student(String name, int rollNo){
      this.name = name;
      this.rollNo = rollNo;
   }
 
   public String getName() {
      return name;
   }
 
   public void setName(String name) {
      this.name = name;
   }
 
   public int getRollNo() {
      return rollNo;
   }
 
   public void setRollNo(int rollNo) {
      this.rollNo = rollNo;
   }
}