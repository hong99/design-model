package com.hong.base.other_type_model.mvc_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 15:02
 * @Description: mvc 用户查看新闻
 */
public class Client {

    public static void main(String[] args) {
        NewsModel newsModel = new NewsModel("中央新闻","12456497894");
        NewsModel newsMode2 = new NewsModel("中央新闻2","12456497894");
        NewsView newsView = new NewsView();
        NewsView newsView2 = new NewsView();
        ViewController viewController = new ViewController(newsModel,newsView);
        viewController.viewNews();
        ViewController viewController2 = new ViewController(newsMode2,newsView2);
        viewController2.viewNews();
    }
}
