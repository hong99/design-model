package com.hong.base.other_type_model.mvc_model.demo;

import java.io.Serializable;

/**
 * @Auther: csh
 * @Date: 2020/6/23 14:53
 * @Description:新闻信息 模型层
 */
public class NewsModel implements Serializable {
    private String title;
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NewsModel(String title, String text) {
        this.title = title;
        this.text = text;
    }
}
