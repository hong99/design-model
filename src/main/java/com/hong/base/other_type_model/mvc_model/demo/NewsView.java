package com.hong.base.other_type_model.mvc_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 14:58
 * @Description: 视图层(view)
 */
public class NewsView {

    public void viewNews(NewsModel model){
        System.out.println("新闻标题:"+model.getTitle());
        System.out.println("新闻内容:"+model.getText());
    }
}
