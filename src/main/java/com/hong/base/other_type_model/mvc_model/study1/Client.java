package com.hong.base.other_type_model.mvc_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/23 10:44
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        //从数据库获取学生记录
        Student model = retrieveStudentFromDatabase();
        //创建一个视频:把学生详细信息输出到控制台
        StudentView studentView = new StudentView();

        StudentController studentController = new StudentController(model, studentView);

        studentController.updateView();

        //更新模型数据
        studentController.setStudentName("John");
        studentController.updateView();

    }

    private static Student retrieveStudentFromDatabase(){
        Student student = new Student();
        student.setName("Robert");
        student.setRollNo("10");
        return student;
    }
}
