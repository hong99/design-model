package com.hong.base.other_type_model.mvc_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/23 15:01
 * @Description: 控制器(controller)
 */
public class ViewController {
    private NewsModel model;
    private NewsView view;

    public ViewController(NewsModel model, NewsView view) {
        this.model = model;
        this.view = view;
    }

    public void viewNews(){
        view.viewNews(model);
    }
}
