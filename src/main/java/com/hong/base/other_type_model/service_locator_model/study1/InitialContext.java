package com.hong.base.other_type_model.service_locator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/7/5 09:55
 * @Description:初始的内容（ Context ）
 */
public class InitialContext {
    public Object lookup(String jniName){
        if(jniName.equalsIgnoreCase("SERVICE1")){
            System.out.println("Looking up and creating a new Service1 object");
            return new Service1();
        }else if(jniName.equalsIgnoreCase("SERVICE2")){
            System.out.println("Looking up and creating a new Service2 object");
            return new Service2();
        }
        return null;
    }
}
