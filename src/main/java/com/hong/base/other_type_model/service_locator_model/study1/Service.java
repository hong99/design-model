package com.hong.base.other_type_model.service_locator_model.study1;
/**
 *
 * 功能描述:抽象的服务接口
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/7/5 11:36
 */
public interface Service {
   public String getName();
   public void execute();
}