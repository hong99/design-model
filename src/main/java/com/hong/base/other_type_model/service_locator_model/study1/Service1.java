package com.hong.base.other_type_model.service_locator_model.study1;
/**
 *
 * 功能描述:服务实现 （Service）
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/7/5 9:54
 */
public class Service1 implements Service {
    @Override
   public void execute(){
      System.out.println("Executing Service1");
   }
 
   @Override
   public String getName() {
      return "Service1";
   }
}