package com.hong.base.other_type_model.service_locator_model.study1;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/5 10:09
 * @Description:
 */
public class Cache {
    private List<Service> services;

    public Cache() {
        services = new ArrayList <Service>();
    }

    public Service getService(String serviceName){
        for (Service service : services) {
            if(service.getName().equalsIgnoreCase(serviceName)){
                System.out.println("Returning cached "+serviceName+"object");
                return service;
            }
        }
        return null;
    }

    public void addServer(Service newService){
        boolean exists = false;
        for (Service service : services) {
            if(service.getName().equalsIgnoreCase(newService.getName())){
                exists = true;
            }
        }
        if(!exists){
            services.add(newService);
        }

    }

}
