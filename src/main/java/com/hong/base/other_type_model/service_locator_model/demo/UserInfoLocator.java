package com.hong.base.other_type_model.service_locator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/5 12:06
 * @Description:服务定位器（Service Locator）
 */
public class UserInfoLocator {
    private static UserCache userCache = new UserCache();

    public static IUserInfo getUserInfo(String userName){
        IUserInfo userInfo = userCache.getUserInfo(userName);
        if(userInfo!=null){
            return userInfo;
        }
        InitialContext instance = InitialContext.getInstance();
        IUserInfo user  = (IUserInfo)instance.lookup(userName);
        userCache.addUserInfo(user);
        return user;
    }
}
