package com.hong.base.other_type_model.service_locator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/5 11:48
 * @Description:创建对象(Context )
 */
public class InitialContext {
    private static InitialContext initialContext = new InitialContext();

    private InitialContext(){

    }

    public static InitialContext getInstance(){
        return initialContext;
    }

    public Object lookup(String username){
        return new User(username);
    }
}
