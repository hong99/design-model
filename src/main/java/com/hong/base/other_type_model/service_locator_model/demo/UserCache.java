package com.hong.base.other_type_model.service_locator_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/5 11:51
 * @Description:用户缓存
 */
public class UserCache {
    private List<IUserInfo> userInfos;

    public UserCache() {
        userInfos = new ArrayList <IUserInfo>();
    }

    public IUserInfo getUserInfo(String userName){
        for (IUserInfo userInfo : userInfos) {
            if(userInfo.getName().equalsIgnoreCase(userName)){
                System.out.println("从缓存中获取到用户信息："+userInfo.toString());
                return userInfo;
            }
        }
        return null;
    }

    public void addUserInfo(IUserInfo userInfo){
        boolean exist = false;
        for (IUserInfo info : userInfos) {
            if(info.getName().equalsIgnoreCase(userInfo.getName())){
                exist = true;
            }
        }
        if(!exist){
            userInfos.add(userInfo);
        }
    }
}
