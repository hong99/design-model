package com.hong.base.other_type_model.service_locator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/5 11:45
 * @Description:用户信息(Service)
 */
public class User implements IUserInfo {
    private String userName;


    public void setUserName(String userName) {
        this.userName = userName;
    }

    private User() {
    }



    public User(String usreName) {
        this.userName = usreName;
    }

    @Override
    public String getName() {
        return userName;
    }

    @Override
    public void printUserInfo() {
        System.out.println("用户信息:"+this.getName());
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                '}';
    }
}
