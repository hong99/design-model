package com.hong.base.other_type_model.service_locator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/7/5 10:23
 * @Description:定位器(Service Locator)
 */
public class ServiceLocator {
    private static Cache cache;
    static {
        cache = new Cache();
    }
    public static Service getService(String jndiName){
        Service service = cache.getService(jndiName);
        if(service!=null){
            return service;
        }
        InitialContext context =new InitialContext();
        Service service1 = (Service)context.lookup(jndiName);
        cache.addServer(service1);
        return service1;
    }
}
