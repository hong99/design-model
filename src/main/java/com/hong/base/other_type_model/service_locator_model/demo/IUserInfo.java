package com.hong.base.other_type_model.service_locator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/5 11:44
 * @Description:抽象的服务接口
 */
public interface IUserInfo {
    public String getName();
    public void printUserInfo();
}
