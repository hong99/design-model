package com.hong.base.other_type_model.service_locator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/7/5 10:25
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Service service = ServiceLocator.getService("Service1");
        service.execute();
        service = ServiceLocator.getService("Service2");
        service.execute();
        service = ServiceLocator.getService("Service1");
        service.execute();
        service =ServiceLocator.getService("Service2");
        service.execute();
    }
}
