package com.hong.base.other_type_model.service_locator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/5 12:51
 * @Description:服务定位器模式
 */
public class Client {
    public static void main(String[] args) {
        IUserInfo userInfo = UserInfoLocator.getUserInfo("user1");
        userInfo.printUserInfo();
        userInfo = UserInfoLocator.getUserInfo("user2");
        userInfo.printUserInfo();
        userInfo = UserInfoLocator.getUserInfo("user1");
        userInfo.printUserInfo();
    }
}
