package com.hong.base.other_type_model.intercepting_filter_model.study;

/**
 * @Auther: csh
 * @Date: 2020/7/3 10:25
 * @Description:演示拦截过滤器设计模式
 */
public class DemoTest {

    public static void main(String[] args) {
        FilterManager filterManager = new FilterManager(new Target());
        filterManager.setFilter(new AuthenticationFilter());
        filterManager.setFilter(new DebugFilter());

        Client client = new Client();
        client.setFilterManager(filterManager);
        client.sendRequest("HOME");
    }
}
