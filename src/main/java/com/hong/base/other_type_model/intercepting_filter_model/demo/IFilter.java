package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:13
 * @Description:抽象过滤器(abstract filter)
 */
public interface IFilter {
    public void execute(String request);
}
