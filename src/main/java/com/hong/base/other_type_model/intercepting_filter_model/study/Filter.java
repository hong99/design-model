package com.hong.base.other_type_model.intercepting_filter_model.study;

/**
 * @Auther: csh
 * @Date: 2020/7/3 09:51
 * @Description:抽象过滤器
 */
public interface Filter {
    public void execute(String request);
}
