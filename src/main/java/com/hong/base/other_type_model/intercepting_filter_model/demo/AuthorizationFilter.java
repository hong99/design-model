package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:20
 * @Description: 权限过滤器(filter)
 */
public class AuthorizationFilter implements IFilter {
    @Override
    public void execute(String request) {
        if(request.contains("admin")){
            System.out.println("进入管理后台");
        }else{
            System.out.println("没有权限");
        }
    }
}
