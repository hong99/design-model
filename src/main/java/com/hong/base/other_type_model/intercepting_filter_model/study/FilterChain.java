package com.hong.base.other_type_model.intercepting_filter_model.study;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/3 09:57
 * @Description:拦截链(Filter Chain)
 */
public class FilterChain {
    private List<Filter> filters = new ArrayList <Filter>();
    private Target target;



    public void addFilter(Filter filter) {
        filters.add(filter);
    }

    public void execute(String request){
        for (Filter filter : filters) {
            filter.execute(request);
        }
        target.execute(request);
    }

    public void setTarget(Target target) {
        this.target = target;
    }
}
