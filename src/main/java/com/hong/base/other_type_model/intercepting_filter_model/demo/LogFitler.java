package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:21
 * @Description:日志记录过滤器(filter)
 */
public class LogFitler implements IFilter {
    @Override
    public void execute(String request) {
        System.out.println("日志记录:"+request);
    }
}
