package com.hong.base.other_type_model.intercepting_filter_model.study;
/**
 *
 * 功能描述:对象处理程序
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/7/3 9:57
 */
public class Target {
   public void execute(String request){
      System.out.println("Executing request: " + request);
   }
}