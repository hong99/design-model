package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:40
 * @Description:用户
 */
public class Client {
    FilterManager filterManager;

    public void setFilterManager(FilterManager filterManager){
        this.filterManager = filterManager;
    }

    public void sendRequest(String request){
        filterManager.filterRequest(request);
    }

}
