package com.hong.base.other_type_model.intercepting_filter_model.study;

/**
 *
 * 功能描述:具体过滤器(Filter)
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/7/3 9:56
 */
public class DebugFilter implements Filter {
   @Override
   public void execute(String request){
      System.out.println("request log: " + request);
   }
}