package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:30
 * @Description:请求进行处理
 */
public class Target {
    public void execute(String request){
        String user = request.contains("admin")?"管理员":"用户";
        System.out.println(user+"进行登陆");
    }
}
