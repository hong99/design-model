package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:41
 * @Description:
 */
public class Test {
    public static void main(String[] args) {
        FilterManager filterManager = new FilterManager(new Target());
        filterManager.setFilter(new AuthorizationFilter());
        filterManager.setFilter(new LogFitler());

        Client client = new Client();
        client.setFilterManager(filterManager);
        client.sendRequest("admin");
        client.sendRequest("USER");
    }
}
