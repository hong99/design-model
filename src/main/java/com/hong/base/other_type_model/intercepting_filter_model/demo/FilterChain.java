package com.hong.base.other_type_model.intercepting_filter_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:31
 * @Description:过滤链
 */
public class FilterChain {
    private List<IFilter> filters = new ArrayList <IFilter>();
    private Target target;

    public void addFilter(IFilter filter){
        filters.add(filter);
    }

    public void excute(String request){
        for (IFilter filter : filters) {
            filter.execute(request);
        }
        target.execute(request);
    }

    public void setTarget(Target target){
        this.target = target;
    }
}
