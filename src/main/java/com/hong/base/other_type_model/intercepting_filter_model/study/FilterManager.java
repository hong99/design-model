package com.hong.base.other_type_model.intercepting_filter_model.study;

/**
 * @Auther: csh
 * @Date: 2020/7/3 10:21
 * @Description:过滤管理器(Filter Manager)
 */
public class FilterManager {

    FilterChain filterChain;

    public FilterManager(Target target) {
        filterChain =new FilterChain();
        filterChain.setTarget(target);
    }

    public void setFilter(Filter filter){
        filterChain.addFilter(filter);
    }

    public void filterRequest(String request){
        filterChain.execute(request);
    }
}
