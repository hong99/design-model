package com.hong.base.other_type_model.intercepting_filter_model.study;

/**
 * @Auther: csh
 * @Date: 2020/7/3 10:23
 * @Description:客户端(client)
 */
public class Client {
    FilterManager filterManager = new FilterManager(new Target());

    public void setFilterManager(FilterManager filterManager){
        this.filterManager = filterManager;
    }

    public void sendRequest(String request){
        filterManager.filterRequest(request);
    }
}
