package com.hong.base.other_type_model.intercepting_filter_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/7/3 15:37
 * @Description:过滤管理器
 */
public class FilterManager {
    FilterChain filterChain;

    public FilterManager(Target target){
        filterChain = new FilterChain();
        filterChain.setTarget(target);
    }

    public void setFilter(IFilter filter) {
        filterChain.addFilter(filter);
    }

    public void filterRequest(String request){
        filterChain.excute(request);
    }
}
