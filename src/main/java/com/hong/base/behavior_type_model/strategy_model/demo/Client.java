package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 16:13
 * @Description:策略模式
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context(new CommonMember());
        System.out.println("普通会员价格:"+context.executeStrategy(100));
        context = new Context(new VipMember());
        System.out.println("Vip会员价格:"+context.executeStrategy(100));
        context = new Context(new GoldMember());
        System.out.println("黄金会员价格:"+context.executeStrategy(100));
    }
}
