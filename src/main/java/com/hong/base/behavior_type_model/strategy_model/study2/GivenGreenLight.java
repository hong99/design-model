package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:51
 * @Description:具体策略角色
 */
public class GivenGreenLight implements IStrategy {
    @Override
    public void operate() {
        System.out.println("求吴国太开绿灯，放行！");
    }
}
