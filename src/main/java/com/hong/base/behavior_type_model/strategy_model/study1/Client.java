package com.hong.base.behavior_type_model.strategy_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:29
 * @Description:策略模式实现
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.println("1+10="+context.executeStrategy(1,10));

        context = new Context(new OperationSubtract());
        System.out.println("11-5="+context.executeStrategy(11,5));

        context = new Context(new OperationMultiply());
        System.out.println("100*3="+context.executeStrategy(100,3));
    }
}
