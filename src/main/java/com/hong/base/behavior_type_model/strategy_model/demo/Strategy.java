package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 15:22
 * @Description:抽象策略
 */
public interface Strategy {
    //打折接口
    Double doOperation(int price);
}
