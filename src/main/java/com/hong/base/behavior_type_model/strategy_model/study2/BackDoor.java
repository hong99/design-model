package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:46
 * @Description:乔国老开后门()
 */
public class BackDoor implements IStrategy {
    @Override
    public void operate() {
        System.out.println("我是乔国老帮忙，让吴国太给孙权施加压力");
    }
}
