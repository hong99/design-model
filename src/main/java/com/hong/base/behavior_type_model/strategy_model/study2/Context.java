package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:52
 * @Description:
 */
public class Context {
    private IStrategy strategy;

    public Context(IStrategy strategy) {
        this.strategy = strategy;
    }

    //使用计谋
    public void operate(){
        this.strategy.operate();
    }
}
