package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 15:52
 * @Description:VIP会员(ConcreteStrategy)
 */
public class VipMember implements Strategy {
    @Override
    public Double doOperation(int price) {
        return price*0.5;
    }
}
