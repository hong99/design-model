package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 15:25
 * @Description:普通会员(ConcreteStrategy)
 */
public class CommonMember implements Strategy {
    @Override
    public Double doOperation(int price) {
        return (price*0.8);
    }
}
