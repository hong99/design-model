package com.hong.base.behavior_type_model.strategy_model.study1;
/**
 *
 * 功能描述:上下文
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/17 14:29
 */
public class Context {
   private Strategy strategy;
 
   public Context(Strategy strategy){
      this.strategy = strategy;
   }
 
   public int executeStrategy(int num1, int num2){
      return strategy.doOperation(num1, num2);
   }
}