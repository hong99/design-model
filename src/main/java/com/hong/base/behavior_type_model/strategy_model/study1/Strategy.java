package com.hong.base.behavior_type_model.strategy_model.study1;
/**
 *
 * 功能描述:抽象策略角色
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/17 14:28
 */
public interface Strategy {
   public int doOperation(int num1, int num2);
}