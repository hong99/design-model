package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:51
 * @Description:
 */
public class BlockEnemy implements IStrategy {
    @Override
    public void operate() {
        System.out.println("孙夫人断后，挡住逃兵");
    }
}
