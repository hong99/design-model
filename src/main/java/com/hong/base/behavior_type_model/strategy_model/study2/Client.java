package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:53
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Context context;
        //刚刚到吴国的时候拆第一个锦囊
        System.out.println("刚刚到吴国的时候拆第一个锦囊");
        context = new Context(new BackDoor());
        context.operate();
        System.out.println("-----------------");
        //刘备乐不思蜀，拆第二个锦囊
        System.out.println("刘备乐不思蜀，拆第二个锦囊");
        context =new Context(new GivenGreenLight());
        context.operate();
        System.out.println("-----------------");
        //孙权的小兵追了，咋办？拆第三个锦囊
        System.out.println("孙权的小兵追了，咋办？拆第三个锦囊");
        context = new Context(new BlockEnemy());
        context.operate();


    }
}
