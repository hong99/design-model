package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 15:54
 * @Description:上下文
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public Double executeStrategy(int price){
        return strategy.doOperation(price);
    }


}
