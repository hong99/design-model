package com.hong.base.behavior_type_model.strategy_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 14:43
 * @Description:抽象策略角色
 */
public interface IStrategy {
    //每个锦囊妙计都是一个可执行的算法
    public void operate();
}
