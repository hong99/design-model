package com.hong.base.behavior_type_model.strategy_model.study1;
/**
 *
 * 功能描述:具体策略
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/17 14:29
 */
public class OperationMultiply implements Strategy{
   @Override
   public int doOperation(int num1, int num2) {
      return num1 * num2;
   }
}