package com.hong.base.behavior_type_model.strategy_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 15:53
 * @Description:黄金会员(ConcreteStrategy)
 */
public class GoldMember implements Strategy {
    @Override
    public Double doOperation(int price) {
        return price*0.3;
    }
}
