package com.hong.base.behavior_type_model.status_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/5/9 15:00
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Context context=new Context();
        context.setLiftState(new ClosingState());
        //首先是电梯门开启，人进去
        context.open();
        //然后电梯门关闭
        context.close();
        //再然后，电梯运行起来，向上或者向下
        context.run();
        //最后到达目的地，电梯停下来
        context.stop();
    }
}
