package com.hong.base.behavior_type_model.status_model.study2;

public interface State {
   public void doAction(Context context);
}