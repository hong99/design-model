package com.hong.base.behavior_type_model.status_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/5/9 15:37
 * @Description:
 */
public class OpenningState extends LiftState {
    @Override
    public void open() {
        System.out.println("电梯门开启...");
    }

    @Override
    public void close() {
        //状态修改
        super.content.setLiftState(Context.closingState);
        //动作委托为closeState来执行
        super.content.getLiftState().close();
    }

    @Override
    public void run() {

    }

    @Override
    public void stop() {

    }
}
