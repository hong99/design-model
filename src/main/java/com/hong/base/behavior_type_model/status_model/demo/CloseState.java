package com.hong.base.behavior_type_model.status_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/12 18:48
 * @Description:ConcreteState(状态具体实现)
 */
public class CloseState implements State {
    @Override
    public void operation(Context context) {
        System.out.println("触发关闭电灯!");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "关闭电灯!";
    }
}
