package com.hong.base.behavior_type_model.status_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/5/9 15:35
 * @Description:停止状态
 */
public class StoppingState extends LiftState {
    @Override
    public void open() {
        super.content.setLiftState(Context.openningState);
        super.content.getLiftState().open();
    }

    @Override
    public void close() {

    }

    @Override
    public void run() {
        super.content.setLiftState(Context.runningState);
        super.content.getLiftState().run();
    }

    @Override
    public void stop() {
        System.out.println("电梯停止了...");
    }
    //停止状态关门？电梯门本来就是关着的！

}
