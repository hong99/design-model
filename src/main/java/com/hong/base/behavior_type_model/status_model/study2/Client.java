package com.hong.base.behavior_type_model.status_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/12 11:35
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context();
        StartState startState = new StartState();
        startState.doAction(context);
        System.out.println(context.getState().toString());
        StopState stopState = new StopState();
        stopState.doAction(context);
        System.out.println(context.getState().toString());
    }
}
