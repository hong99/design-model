package com.hong.base.behavior_type_model.status_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/5/9 15:40
 * @Description:关闭状态
 */
public class ClosingState extends LiftState {
    @Override
    public void open() {
        super.content.setLiftState(Context.openningState);
        super.content.getLiftState().open();
    }

    @Override
    public void close() {
        System.out.println("电梯门关闭...");
    }

    @Override
    public void run() {
        super.content.setLiftState(Context.runningState);
        super.content.getLiftState().run();
    }

    @Override
    public void stop() {
        super.content.setLiftState(Context.stoppingState);
        super.content.getLiftState().stop();
    }
}
