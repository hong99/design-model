package com.hong.base.behavior_type_model.status_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/12 18:00
 * @Description:Context(上下文或称环境)
 */
public class Context {
    private State state;


    public Context() {
        state = null;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
