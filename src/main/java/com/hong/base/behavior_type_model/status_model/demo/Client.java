package com.hong.base.behavior_type_model.status_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/12 18:48
 * @Description:演示开关灯通过状态进行变更。
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context();
        OpenState openState = new OpenState();
        openState.operation(context);
        System.out.println(context.getState().toString());
        CloseState closeState = new CloseState();
        closeState.operation(context);
        System.out.println(context.getState().toString());
    }
}
