package com.hong.base.behavior_type_model.status_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/12 18:03
 * @Description:
 */
public interface State {
    //操作
    public void operation(Context context);
}
