package com.hong.base.behavior_type_model.status_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/5/9 15:40
 * @Description:
 */
public class RunningState extends LiftState{
    @Override
    public void open() {

    }

    @Override
    public void close() {

    }

    @Override
    public void run() {
        System.out.println("电梯上下运行");
    }

    @Override
    public void stop() {
        //环境设置为停止状态
        super.content.setLiftState(Context.stoppingState);
        super.content.getLiftState().stop();
    }
}
