package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName Client
 * @Author csh
 * @Description 演示访问者模式，不能人进入不同厕所
 * @Date 2020/6/22
 **/
public class Client {
    public static void main(String[] args) {
        Person man1 = new Male("hong1");
        Person man2 = new Male("hong2");
        Person woman1 =new Woman("min1");
        Person woman2= new Woman("min2");
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.add(man1);
        objectStructure.add(man2);
        objectStructure.add(woman1);
        objectStructure.add(woman2);
        //男厕所
        IVisitor man = new ManVisitor();
        //女厕所
        IVisitor woman = new WomanVisitor();
        //进入男厕所
        objectStructure.action(man);
        //进入女厕所
        objectStructure.action(woman);
    }
}
