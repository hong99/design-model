package com.hong.base.behavior_type_model.visiter_model.study2;

/**
 * @ClassName Client
 * @Author 陈树宏
 * @Description
 * @Date 2020/6/22
 **/
public class Client {
    public static void main(String[] args) {
        ComputerPart computerPart = new Computer();
        computerPart.accept(new ComputerPartDisplayVisitor());
    }
}
