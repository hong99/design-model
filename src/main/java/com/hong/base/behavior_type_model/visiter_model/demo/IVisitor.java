package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName IVisitor
 * @Author csh
 * @Description 抽象访问者（Visitor）
 * @Date 2020/6/22
 **/
public interface IVisitor {
    void visit(Person person);
}
