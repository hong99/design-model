package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName Woman
 * @Author csh  具体元素(ConcreteElement )
 * @Description
 * @Date 2020/6/22
 **/
public class Woman extends Person {

    public Woman(String name) {
        super(name);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
