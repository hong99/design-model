package com.hong.base.behavior_type_model.visiter_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ObjectStructure
 * @Author csh  ObjectStruture(对象结构)
 * @Description
 * @Date 2020/6/22
 **/
public class ObjectStructure {
    private List<Person> personList = new ArrayList<Person>();

    public void action(IVisitor visitor){
        for (Person person : personList) {
            person.accept(visitor);
        }
    }

    public void add(Person person){
        personList.add(person);
    }

    public void remove(Person person){
        personList.remove(person);
    }
}
