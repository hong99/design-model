package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName ManVisitor
 * @Author csh  具体访问者(ConcreteVisitor)
 * @Description
 * @Date 2020/6/22
 **/
public class ManVisitor implements IVisitor {
    @Override
    public void visit(Person person) {
        if(person instanceof Male){
            System.out.println(person.getName()+"进入男厕所！");
        }
    }
}
