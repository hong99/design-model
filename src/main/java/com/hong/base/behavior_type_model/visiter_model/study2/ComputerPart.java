package com.hong.base.behavior_type_model.visiter_model.study2;

public interface ComputerPart {
   public void accept(ComputerPartVisitor computerPartVisitor);
}