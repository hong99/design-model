package com.hong.base.behavior_type_model.visiter_model.study1;

public interface Visitor {

    // 访问工程师类型
    void visit(Engineer engineer);

    // 访问经理类型
    void visit(Manager manager);
}