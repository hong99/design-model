package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName Male
 * @Author csh
 * @Description 具体元素(ConcreteElement )
 * @Date 2020/6/22
 **/
public class Male extends Person {

    public Male(String name) {
        super(name);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
