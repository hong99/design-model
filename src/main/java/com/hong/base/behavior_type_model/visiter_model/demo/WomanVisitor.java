package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName WomanVisitor
 * @Author csh  具体访问者(ConcreteVisitor)
 * @Description
 * @Date 2020/6/22
 **/
public class WomanVisitor implements IVisitor {
    @Override
    public void visit(Person person) {
        if(person instanceof Woman){
            System.out.println(person.getName()+"进入女厕所!");
        }
    }
}
