package com.hong.base.behavior_type_model.visiter_model.demo;

/**
 * @ClassName Person
 * @Author csh
 * @Description 人 抽象元素(Element)
 * @Date 2020/6/22
 **/
public abstract class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //抽象访问接口
    public abstract void accept(IVisitor visitor);
}
