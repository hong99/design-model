package com.hong.base.behavior_type_model.observer_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/11 11:04
 * @Description:被观查者
 */
public  class Subject {
    protected List<ObServer> obServerList = new ArrayList <ObServer>();

    private String message;
    //添加订阅者
    public void attach(ObServer obServer){
        obServerList.add(obServer);
    }
    //删除订阅者
    public void del(ObServer obServer){
        if(null!=obServer){
            obServerList.remove(obServer);
        }
    }
    //通知所有订阅者
    public void notifyAllObservers(String message){
        for (ObServer obServer : obServerList) {
            obServer.update(message);
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
