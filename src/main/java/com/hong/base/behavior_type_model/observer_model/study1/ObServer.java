package com.hong.base.behavior_type_model.observer_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 16:50
 * @Description:观察者
 */
public abstract class ObServer {
    protected Subject subject;
    public abstract void update();
}
