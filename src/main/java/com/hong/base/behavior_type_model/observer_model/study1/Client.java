package com.hong.base.behavior_type_model.observer_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/11 09:38
 * @Description:
 *  观察者通过监听对象的行为进行通知。
 */
public class Client {
    public static void main(String[] args) {
        Subject subject = new Subject();
        new HexaObserver(subject);
        new BinaryObserver(subject);
        new OctalObserver(subject);
        System.out.println("First state change 15");
        subject.setState(15);
        System.out.println("Second state change 10");
        subject.setState(10);

    }
}
