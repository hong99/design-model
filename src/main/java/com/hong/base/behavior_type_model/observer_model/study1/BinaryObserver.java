package com.hong.base.behavior_type_model.observer_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 16:53
 * @Description:具体观察者
 */
public class BinaryObserver extends ObServer {

    public BinaryObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("Binary String："+Integer.toBinaryString(subject.getState()));
    }
}
