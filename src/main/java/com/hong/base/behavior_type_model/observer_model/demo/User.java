package com.hong.base.behavior_type_model.observer_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/11 11:12
 * @Description:用户(具体观察者)
 */
public class User extends ObServer {

   private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        System.out.println("用户:"+name+"收到通知："+message);
    }
}
