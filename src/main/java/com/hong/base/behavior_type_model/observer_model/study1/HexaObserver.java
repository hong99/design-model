package com.hong.base.behavior_type_model.observer_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 16:53
 * @Description:具体观察者
 */
public class HexaObserver extends ObServer {

    public HexaObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("Hex String："+Integer.toHexString(subject.getState()).toUpperCase());
    }
}
