package com.hong.base.behavior_type_model.observer_model.demo;

import com.hong.base.behavior_type_model.observer_model.study1.Subject;

/**
 * @Auther: csh
 * @Date: 2020/6/11 11:03
 * @Description:抽象观察者(ObServer)
 */
public abstract class ObServer {

    protected Subject subject;
    public abstract void update(String message);
}
