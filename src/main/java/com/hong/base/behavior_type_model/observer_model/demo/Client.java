package com.hong.base.behavior_type_model.observer_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/11 11:18
 * @Description:观察者模式，用户观注的公众号后，公众号更新文章通知所有用户。
 */
public class Client {
    public static void main(String[] args) {
        //关注【技术趋势】公众号
        Subject jsqs = new Subject();
        //用户
        User user = new User("小明");
        User user2 = new User("小红");
        User user3 = new User("小绿");
        //订阅公众号
        jsqs.attach(user);
        jsqs.attach(user2);
        jsqs.attach(user3);
        //公众号更新文章
        jsqs.notifyAllObservers("观察者模式");
    }
}
