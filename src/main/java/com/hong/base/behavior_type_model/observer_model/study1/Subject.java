package com.hong.base.behavior_type_model.observer_model.study1;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/10 16:49
 * @Description:被观察者
 */
public class Subject {
    private List<ObServer> obServerList=new ArrayList <ObServer>();
    private int state;


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        notifyAllObservers();
    }

    //添加
    public void attach(ObServer obServer){
        if(null!=obServer){
            obServerList.add(obServer);
        }
    }
    //通知
    public void notifyAllObservers(){
        for (ObServer obServer : obServerList) {
            obServer.update();
        }
    }
}
