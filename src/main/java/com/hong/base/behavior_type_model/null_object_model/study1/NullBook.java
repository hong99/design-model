package com.hong.base.behavior_type_model.null_object_model.study1;
/**
 *
 * 功能描述:空对象
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/17 9:33
 */
public class NullBook implements Book {
    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public void show() {
        System.out.println("Sorry，未找到符合您输入的ID图像信息，请确认输入的值是否合法。");
    }
}