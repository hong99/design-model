package com.hong.base.behavior_type_model.null_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:21
 * @Description:空对象(NullObject)
 */
public class NullTv implements Tv {
    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public String getTvStation() {
        return "该电视台不存在，请切换其他电视台观看！";
    }
}
