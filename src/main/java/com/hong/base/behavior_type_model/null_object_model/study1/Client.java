package com.hong.base.behavior_type_model.null_object_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/17 09:35
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        BookFactory bookFactory = new BookFactory();
        Book book = bookFactory.getBook(-1);
        if(book.isNull()){
            System.out.println("输入ID不规范！");
        }else{
            book.show();
        }
    }
}
