package com.hong.base.behavior_type_model.null_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:22
 * @Description:具体实现(RealObject)
 */
public class ViewTv implements Tv {

    private String name;

    private Integer number;

    public ViewTv(String name, Integer number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getTvStation() {
        return "为您播放"+name+"台数"+number;
    }
}
