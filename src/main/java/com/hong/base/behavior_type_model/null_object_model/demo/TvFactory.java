package com.hong.base.behavior_type_model.null_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:24
 * @Description:电视台工厂（Factory）
 */
public class TvFactory {

    public Tv getTv(Integer number){
        Tv tv;
        switch (number){
            case 1:
                tv = new ViewTv("中央一台",1);
                break;
            case 2:
                tv = new ViewTv("中央二台",2);
            default:
                tv = new NullTv();
                break;
        }
        return tv;
    }
}
