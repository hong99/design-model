package com.hong.base.behavior_type_model.null_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:26
 * @Description:电视台演示
 */
public class Client {
    public static void main(String[] args) {
        TvFactory tvFactory = new TvFactory();
        Tv tv = tvFactory.getTv(1);
        if(!tv.isNull()){
            System.out.println(tv.getTvStation());
        }else{
            System.out.println("电视台不存在！");
        }
        Tv tv1 = tvFactory.getTv(111);
        if(!tv1.isNull()){
            System.out.println(tv1.getTvStation());
        }else{
            System.out.println("电视台不存在！");
        }

    }
}
