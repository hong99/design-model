package com.hong.base.behavior_type_model.null_object_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:03
 * @Description:空对象
 */
public class CustomerFactory {

    public static final String[] names= {"Rob", "Joe", "Julie"};

    public static AbstractCustomer getCustomer(String name) {
        for (int i = 0; i < names.length; i++) {
            if(names[i].equalsIgnoreCase(name)){
                return new RealCustomer(name);
            }
        }
        return new NullCustomer();
    }


}
