package com.hong.base.behavior_type_model.null_object_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/17 09:32
 * @Description:书抽象
 */
public interface Book {
    // 判断Book对象是否为空对象（Null Object）
    public boolean isNull();

    // 展示Book对象的信息内容。
    public void show();
}
