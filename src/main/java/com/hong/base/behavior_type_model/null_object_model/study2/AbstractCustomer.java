package com.hong.base.behavior_type_model.null_object_model.study2;

public abstract class AbstractCustomer {
   protected String name;
   public abstract boolean isNil();
   public abstract String getName();
}