package com.hong.base.behavior_type_model.null_object_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/17 10:20
 * @Description:(AbstractObject)抽象接口
 */
public interface Tv {
    //是否为空
    boolean isNull();
    //电视台
    String getTvStation();
}
