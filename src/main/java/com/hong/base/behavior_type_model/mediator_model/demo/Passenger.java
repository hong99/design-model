package com.hong.base.behavior_type_model.mediator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 18:19
 * @Description:乘客【Colleagueclass(具体同事角色)】
 */
public class Passenger extends AbstractColleague {

    public Passenger(String name, AbstractMediator abstractMediator) {
        super(name, abstractMediator);
    }

    @Override
    protected void sendMessage(String msg) {
        this.abstractMediator.commission(this,msg);
    }

    @Override
    protected void getMessage(String msg) {
        System.out.println("乘客【"+name+"】收到中介发来的消息"+msg);
    }
}
