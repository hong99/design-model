package com.hong.base.behavior_type_model.mediator_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:48
 * @Description:抽象同事(Colleague)
 */
public abstract class Person {
    //中介
    protected Mediator mediator;

    protected String name;

    public Person(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }


    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    /**
     * 向中介 发送消息
     */
    protected abstract void sendMessage(String msg);

    /**
     * 从中介 获取消息
     */
    protected abstract void getMessage(String msg);

}
