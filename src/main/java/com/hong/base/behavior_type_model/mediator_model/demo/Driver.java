package com.hong.base.behavior_type_model.mediator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 18:16
 * @Description:司机 具体同事实现(Colleagueclass)
 */
public class Driver extends AbstractColleague {


    public Driver(String name, AbstractMediator abstractMediator) {
        super(name, abstractMediator);
    }

    @Override
    protected void sendMessage(String msg) {
        this.abstractMediator.commission(this,msg);
    }

    @Override
    protected void getMessage(String msg) {
        System.out.println("司机【"+this.name+"】收到中介发来的消息:"+msg);
    }
}
