package com.hong.base.behavior_type_model.mediator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 18:20
 * @Description:代理模式，模拟顺风车，乘客发部信息，司机可以收到，而司机发部信息乘客也可收到。
 */
public class Client {
    public static void main(String[] args) {
        //滴滴代理平台
        AbstractMediator didi = new Didi();
        AbstractColleague driver = new Driver("司机1",didi);
        AbstractColleague driver2 = new Driver("司机2",didi);
        AbstractColleague passenger = new Passenger("乘客1",didi);
        AbstractColleague passenger2 = new Passenger("乘客2",didi);
        didi.addDriver(driver);
        didi.addDriver(driver2);
        didi.addPassenger(passenger);
        didi.addPassenger(passenger2);
        driver.sendMessage("有没有去北京？");
        passenger.sendMessage("我需要打车去北京!");
    }
}
