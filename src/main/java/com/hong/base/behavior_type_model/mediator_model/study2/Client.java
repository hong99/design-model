package com.hong.base.behavior_type_model.mediator_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:59
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        //中介
        Mediator mediator = new HouseMediator();

        Person landlordA,landlordB,renter;
        landlordA = new Landlord(mediator,"房东陈");
        landlordB = new Landlord(mediator,"房东卢");

        renter = new Renter(mediator,"小卢");

        //房东注册中介
        mediator.registerLandlord(landlordA);
        mediator.registerLandlord(landlordB);

        //求租者注册中介
        mediator.registerRenter(renter);

        //求租者 发送求租信息
        renter.sendMessage("找宝安中心 一房一厅2000");
        System.out.println("----------------");
        //房东发出租信息
        landlordA.sendMessage("宝安中心出租 一房一厅 18000");
    }
}
