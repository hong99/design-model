package com.hong.base.behavior_type_model.mediator_model.study1;

import java.io.Serializable;

/**
 * @Auther: csh
 * @Date: 2020/6/9 14:42
 * @Description:用户
 */
public class User implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User(String name) {
        this.name = name;
    }

    public void sendMessage(String message){
        ChatRoom.showMessage(this,message);
    }
}
