package com.hong.base.behavior_type_model.mediator_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:17
 * @Description:抽象中介者(mediator)
 */
public abstract class AbstractMediator {
    //存放 "乘客信息"
    protected List<AbstractColleague> passengerList = new ArrayList <AbstractColleague>();
    //存放 "司机信息"
    protected List<AbstractColleague> driverList = new ArrayList <AbstractColleague>();


    //添加乘客
    public void addPassenger(AbstractColleague passenger){
        passengerList.add(passenger);
    }

    //添加司机
    public void addDriver(AbstractColleague passenger){
        driverList.add(passenger);
    }
    //由具体中介者子类实现 消息的中转和协调
    public abstract void commission(AbstractColleague passenger,String work);
}
