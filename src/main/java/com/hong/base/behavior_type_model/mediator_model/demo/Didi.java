package com.hong.base.behavior_type_model.mediator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 18:20
 * @Description:平台 具体中介角色(ConcreteMediator)
 */
public class Didi extends AbstractMediator {

    @Override
    public void commission(AbstractColleague passenger, String work) {
        if(passenger instanceof Driver){
            //将匹配到的司机发送给乘客
            for (AbstractColleague abstractColleague : passengerList) {
                abstractColleague.getMessage(work);
            }
        }else{
            //将需要打车的乘客发给司机
            for (AbstractColleague abstractColleague : driverList) {
                abstractColleague.getMessage(work);
            }
        }
    }
}
