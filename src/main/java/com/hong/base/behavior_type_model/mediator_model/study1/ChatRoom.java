package com.hong.base.behavior_type_model.mediator_model.study1;

import java.util.Date;

/**
 * @Auther: csh
 * @Date: 2020/6/9 14:41
 * @Description:聊天窗
 */
public class ChatRoom {

    public static void showMessage(User user,String message){
        System.out.println(new Date().toString()
                + " [" + user.getName() +"] : " + message);
    }
}
