package com.hong.base.behavior_type_model.mediator_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:50
 * @Description:具体同事类角色
 */
public class Renter extends Person {

    public Renter(Mediator mediator, String name) {
        super(mediator, name);
    }

    @Override
    protected void sendMessage(String msg) {
        this.mediator.operation(this,msg);
    }

    @Override
    protected void getMessage(String msg) {
        System.out.println("求租者[" + name + "]收到中介发来的消息： " + msg);

    }
}
