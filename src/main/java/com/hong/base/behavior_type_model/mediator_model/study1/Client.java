package com.hong.base.behavior_type_model.mediator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/9 14:43
 * @Description:通过一个聊天窗()展消息展示
 */
public class Client {
    public static void main(String[] args) {
        User rober = new User("rober");
        User john = new User("john");
        rober.sendMessage("hell");
        john.sendMessage("hi rober");

    }
}
