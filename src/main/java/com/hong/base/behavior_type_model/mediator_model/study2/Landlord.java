package com.hong.base.behavior_type_model.mediator_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:53
 * @Description:
 */
public class Landlord extends Person {
    public Landlord(Mediator mediator, String name) {
        super(mediator, name);
    }

    @Override
    protected void sendMessage(String msg) {
        mediator.operation(this, msg);
    }

    @Override
    protected void getMessage(String msg) {
        System.out.println("房东["+ name +"]收到中介发来的消息：" + msg);
    }
}
