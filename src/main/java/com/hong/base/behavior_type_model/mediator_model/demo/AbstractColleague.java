package com.hong.base.behavior_type_model.mediator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 17:37
 * @Description:抽象的同事类
 */
public abstract class AbstractColleague {
    //乘客名称
    protected String name;
    //中介
    protected AbstractMediator abstractMediator;

    public AbstractColleague(String name, AbstractMediator abstractMediator) {
        this.name = name;
        this.abstractMediator = abstractMediator;
    }


    public void setAbstractMediator(AbstractMediator abstractMediator) {
        this.abstractMediator = abstractMediator;
    }

    /**
     * 向中介 发送消息
     */
    protected abstract void sendMessage(String msg);

    /**
     * 从中介 获取消息
     */
    protected abstract void getMessage(String msg);


}
