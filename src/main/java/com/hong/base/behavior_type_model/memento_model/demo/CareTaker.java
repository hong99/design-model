package com.hong.base.behavior_type_model.memento_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/10 14:46
 * @Description:Caretaker(备忘录管理员角色)
 */
public class CareTaker {
    //存储备份的列表
    private List<Memento> mementoList = new ArrayList <Memento>();
    //添加
    public void add(Memento memento) {
        if(null!=memento){
            this.mementoList.add(memento);
        }
    }

    //获取方式
    public Memento getToMemento(Memento memento){
        for (Memento mo : mementoList) {
            if(mo.getState().equals(memento.getState())){
                return mo;
            }
        }
        return null;
    }

    //获取
    public Memento get(int index){
        return mementoList.get(index);
    }
    //删除
    public boolean del(Memento memento){
        if(null!=memento){
            return this.mementoList.remove(memento);
        }
        return false;
    }
}
