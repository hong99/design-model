package com.hong.base.behavior_type_model.memento_model.study2;

import com.hong.base.behavior_type_model.memento_model.study1.CareTaker;

/**
 * @Auther: csh
 * @Date: 2020/6/10 11:18
 * @Description:
 */
public class Client {

    public static void main(String[] args) {
        //声明出主角
        Boy boy = new Boy();
        //声明出备忘录的管理者
        Caretaker careTaker = new Caretaker();
        //初始化当前状态
        boy.setState("心情很棒");
        System.out.println("=========男孩子现在的状态============");
        System.out.println(boy.getState());
        //需要记录下当前状态呀
        careTaker.setMemento(boy.createMemento());
        //男孩子去追女孩，状态改变
        boy.chnageState();
        System.out.println("--------男孩子追女孩子后的状态---------------");
        System.out.println(boy.getState());
        //追女孩子失败，恢得原状
        boy.restoreMemento(careTaker.getMemento());
        System.out.println("------男孩子恢复后的状态--------------");
        System.out.println(boy.getState());
    }
}
