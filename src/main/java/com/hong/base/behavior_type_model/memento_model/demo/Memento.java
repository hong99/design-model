package com.hong.base.behavior_type_model.memento_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/10 14:41
 * @Description:Mement(备忘录角色)
 */
public class Memento {
    private String state;

    public Memento(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }
}
