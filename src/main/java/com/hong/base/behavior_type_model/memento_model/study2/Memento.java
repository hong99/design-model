package com.hong.base.behavior_type_model.memento_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/10 10:59
 * @Description:备份
 *
 */
public class Memento {
    //男孩子的状态
    private String state = "";
    //通过构造函数传递状态信息

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
