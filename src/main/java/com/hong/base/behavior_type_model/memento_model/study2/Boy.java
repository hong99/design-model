package com.hong.base.behavior_type_model.memento_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/10 10:57
 * @Description:男孩
 */
public class Boy {
    //男孩的状态
    private String state="";
    //认识女孩子后状态瞬睛改变,
    public void chnageState(){
        this.state="心情可能很不好";
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    //保留一个备份
    public Memento createMemento(){
        return new Memento(this.state);
    }


    //恢复一个备份
    public void restoreMemento(Memento _memenoto){
        this.setState(_memenoto.getState());
    }
}
