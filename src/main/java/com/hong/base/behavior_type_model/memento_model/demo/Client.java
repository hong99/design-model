package com.hong.base.behavior_type_model.memento_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/10 14:51
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        CareTaker careTaker = new CareTaker();
        Person person  = new Person();
        person.setStatus("普通人");
        System.out.println("------保存状态--------");
        Memento memento = person.saveStateToMemento();
        careTaker.add(memento);
        System.out.println("变身前状态:"+person.getStatus());
        System.out.println("--------变身--------");
        person.changeStatus();
        System.out.println("拯救世界去了:"+person.getStatus());
        System.out.println("继续恢复平凡人的身份......");
        person.getStatusFromMemento(careTaker.get(0));
        System.out.println("恢复后:"+person.getStatus());
    }
}
