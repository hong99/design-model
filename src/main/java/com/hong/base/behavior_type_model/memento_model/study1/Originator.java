package com.hong.base.behavior_type_model.memento_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 10:20
 * @Description:
 */
public class Originator {
    private String state;

    public void setState(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }

    public Memento saveStateToMemento(){
        return new Memento(state);
    }

    public void getStateFromMemento(Memento memento){
        state = memento.getState();
    }
}
