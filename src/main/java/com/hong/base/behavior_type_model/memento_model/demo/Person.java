package com.hong.base.behavior_type_model.memento_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/10 14:39
 * @Description:人 （Originator）
 */
public class Person {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void changeStatus(){
        this.status="变身超人";
    }

    //保存状态
    public Memento saveStateToMemento(){
        return new Memento(status);
    }

    //通过记录获取状态
    public void getStatusFromMemento(Memento memento){
        if(null!=memento){
            this.status = memento.getState();
        }else{
            System.out.println("没有状态恢复！");
        }
    }
}
