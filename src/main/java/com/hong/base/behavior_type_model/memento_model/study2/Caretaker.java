package com.hong.base.behavior_type_model.memento_model.study2;

/**
 * @Auther: csh
 * @Date: 2020/6/10 11:40
 * @Description:
 */
public class Caretaker {
    //备忘录对象
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
