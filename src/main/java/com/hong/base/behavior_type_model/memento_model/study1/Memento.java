package com.hong.base.behavior_type_model.memento_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 10:19
 * @Description:
 */
public class Memento {
    private String state;

    public Memento(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }
}
