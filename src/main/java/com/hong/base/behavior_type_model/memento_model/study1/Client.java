package com.hong.base.behavior_type_model.memento_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/10 10:21
 * @Description:
 * 该案例就是将创建的对像放到CareTaker中，CareTaker作缓存用。然后再通过CareTaker去获取完后还原给原来的对象，这样就还原了。
 */
public class Client {
    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();
        originator.setState("state #1");
        originator.setState("state #2");
        careTaker.add(originator.saveStateToMemento());
        originator.setState("state #3");
        careTaker.add(originator.saveStateToMemento());
        originator.setState("State #4");

        System.out.println("current state:"+originator.getState());
        originator.getStateFromMemento(careTaker.get(0));
        System.out.println("First saved State:"+originator.getState());
        originator.getStateFromMemento(careTaker.get(1));
        System.out.println("Second saved State:"+originator.getState());
    }
}
