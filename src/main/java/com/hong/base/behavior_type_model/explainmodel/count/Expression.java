package com.hong.base.behavior_type_model.explainmodel.count;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/5/11 10:23
 * @Description:
 */
public abstract class Expression {
    //解析公或和数值，其中var中的key值是是公式中的参数，value值是具体的数字
    public abstract int interpreter(HashMap<String,Integer> var);
}
