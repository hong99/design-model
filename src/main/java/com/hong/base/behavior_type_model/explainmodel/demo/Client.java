package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 14:42
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        String chinese = "chinese";
        String test = "demo";
        String english = "english";
        Expression chineseExpression = new TerminalExpression(chinese);
        Expression englishExpression = new TerminalExpression(english);
        Expression testExpression = new TerminalExpression(test);
        System.out.println(new LanguageUtil(new EnglishExpression(englishExpression)).out());
        System.out.println(new LanguageUtil(new ChineseExpression(chineseExpression)).out());
        System.out.println(new LanguageUtil(new EnglishExpression(testExpression)).out());
    }
}
