package com.hong.base.behavior_type_model.explainmodel.count;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/5/11 10:28
 * @Description:加法
 */
public class AddExpression extends SysmblExpression {
    public AddExpression(Expression left, Expression right) {
        super(left, right);
    }
    //把左右两个表达式运算的结果加起来
    @Override
    public int interpreter(HashMap<String,Integer> var){
        return super.left.interpreter(var)+super.right.interpreter(var);
    }
}
