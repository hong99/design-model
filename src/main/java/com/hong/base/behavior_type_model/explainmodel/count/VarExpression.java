package com.hong.base.behavior_type_model.explainmodel.count;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/5/11 10:25
 * @Description:
 */
public class VarExpression extends Expression {
    private String key;

    public VarExpression(String key) {
        this.key = key;
    }

    //从map中取之
    @Override
    public int interpreter(HashMap<String,Integer> var){
        return var.get(this.key);
    }
}
