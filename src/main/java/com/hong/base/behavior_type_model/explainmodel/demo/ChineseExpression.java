package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 11:56
 * @Description:非终结符表达式
 */
public class ChineseExpression implements Expression {

    private Expression expression;

    public ChineseExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public String interpret() {
        String interpret = expression.interpret();
        if(interpret.contains("chinese")){
            return "中文";
        }
        return null;
    }
}
