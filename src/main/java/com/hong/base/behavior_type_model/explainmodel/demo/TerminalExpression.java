package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 14:22
 * @Description:终结符表达式
 */
public class TerminalExpression implements Expression {

    private String context;


    public TerminalExpression(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }



    @Override
    public String interpret() {
        return context;
    }

}
