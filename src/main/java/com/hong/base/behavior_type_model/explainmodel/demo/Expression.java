package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 11:42
 * @Description:抽象解释器
 */
public interface Expression {
    /**
     *
     * 功能描述:解释器接口
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/6/8 11:55
     */
    public String interpret();
}
