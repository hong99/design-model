package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 14:12
 * @Description:非终结符表达式
 */
public class EnglishExpression implements Expression {


    private Expression expression;

    public EnglishExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public String interpret() {
        String interpret = expression.interpret();
        if(interpret.contains("english")){
            return "英文";
        }
        return null;
    }
}
