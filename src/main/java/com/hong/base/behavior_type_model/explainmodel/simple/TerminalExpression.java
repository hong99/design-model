package com.hong.base.behavior_type_model.explainmodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/5 17:22
 * @Description:判断包含实现(终结符)
 */
public class TerminalExpression implements Expression {

    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpret(String content) {
        if(content.contains(data)){
            return true;
        }
        return false;
    }
}
