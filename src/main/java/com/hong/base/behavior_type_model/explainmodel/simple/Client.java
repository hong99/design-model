package com.hong.base.behavior_type_model.explainmodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/5 17:26
 * @Description:判断关系
 */
public class Client {

    //规则：小明和小李都是男的
    public static Expression getMaleExpression(){
        Expression xiaoMin = new TerminalExpression("xiaoMin");
        Expression xiaoLi = new TerminalExpression("xiaoLi");
        return new OrExpression(xiaoLi,xiaoMin);
    }
    //规则: 冰冰是一个女性
    public static Expression getBinBinExpression(){
        Expression bingbing = new TerminalExpression("BingBing");
        Expression hong = new TerminalExpression("hong");
        return new AndExpression(bingbing,hong);
    }

    public static void main(String[] args) {
        Expression isMale = getMaleExpression();
        Expression isBingBing = getBinBinExpression();
        System.out.println(isMale.interpret("xiaoMin"));
        System.out.println(isBingBing.interpret("hong"));
    }
}
