package com.hong.base.behavior_type_model.explainmodel.count;

/**
 * @Auther: csh
 * @Date: 2020/5/11 10:26
 * @Description:
 */
public abstract class SysmblExpression extends Expression {
    protected Expression left;
    protected Expression right;
    //所有的解析公式都应只关心自己左右两个表达式的结果

    public SysmblExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }
}
