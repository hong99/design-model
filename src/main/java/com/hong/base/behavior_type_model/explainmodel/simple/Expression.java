package com.hong.base.behavior_type_model.explainmodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/5 17:22
 * @Description:表达式接口
 */
public interface Expression {
    public boolean interpret(String content);
}
