package com.hong.base.behavior_type_model.explainmodel.count;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/5/11 10:29
 * @Description:减法
 */
public  class SubExpression extends SysmblExpression {

    public SubExpression(Expression left, Expression right) {
        super(left, right);
    }
    //左右两个表达式相减
    public int interpreter(HashMap<String,Integer> var){
        return super.left.interpreter(var) - super.right.interpreter(var);
    }
}
