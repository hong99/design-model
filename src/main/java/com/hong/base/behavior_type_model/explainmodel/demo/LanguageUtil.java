package com.hong.base.behavior_type_model.explainmodel.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/8 15:01
 * @Description:解释器工具
 */
public class LanguageUtil{
    private Expression expression;

    public LanguageUtil(Expression expression) {
        this.expression = expression;
    }

    public String out(){
        return expression.interpret()==null?"无法解释":expression.interpret();
    }

}
