package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:35
 * @Description:丈夫类
 */
public class Husband extends Handler {


    public Husband() {
        super(Handler.HUSBAND_LEVEL_REQUEST);
    }

    @Override
    protected void response(IWomen women) {
        System.out.println("妻子的请示是:"+women.getRequest());
        System.out.println("丈夫的答复是:同意");
    }
}
