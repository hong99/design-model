package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:34
 * @Description:
 */
public class Father extends Handler {

    public Father() {
        super(Handler.FATHER_LEVEL_PEQUEST);
    }

    @Override
    protected void response(IWomen women) {
        System.out.println("-----------------女儿向父亲请示----");
        System.out.println(women.getRequest());
        System.out.println("父亲的答复是:同意\n");
    }
}
