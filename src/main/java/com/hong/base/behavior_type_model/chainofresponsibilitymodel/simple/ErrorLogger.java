package com.hong.base.behavior_type_model.chainofresponsibilitymodel.simple;
/**
 *
 * 功能描述:记录器实体类
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/3 18:24
 */
public class ErrorLogger extends AbstractLogger {
 
   public ErrorLogger(int level){
      this.level = level;
   }
 
   @Override
   protected void write(String message) {    
      System.out.println("Error Console::Logger: " + message);
   }
}