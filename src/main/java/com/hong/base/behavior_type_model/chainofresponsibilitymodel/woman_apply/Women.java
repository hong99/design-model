package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:30
 * @Description:古代妇女
 */
public class Women implements IWomen {
    /*
     * 通过一个int类型的参数来描述妇女的个人状况
     * 1--未出嫁
     * 2--出嫁
     * 3--夫死
     */
    private int type=0;
    //妇女的请示
    private String request= "";

    //获得自己的状况
    @Override
    public int getType() {
        return this.type;
    }

    //获取妇女的请求
    @Override
    public String getRequest() {
        return this.request;
    }

    public Women(int type, String request) {
        this.type = type;
        switch (this.type){
            case 1:
                    this.request="女儿的请求是:"+request;
                    break;
            case 2:
                this.request="妻子的请求是:"+request;
                break;
            case 3:
                this.request="母亲的请求是:"+request;
                break;
        }
    }
}
