package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

/**
 * @Auther: csh
 * @Date: 2020/6/4 16:56
 * @Description:抽象处理类
 */
public abstract class Handler {

    //对象
    private Handler nextHandler;


    public Handler getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    /**
     * 员工
     * @param employe
     */
    protected abstract void doHandler(Employe employe);
}
