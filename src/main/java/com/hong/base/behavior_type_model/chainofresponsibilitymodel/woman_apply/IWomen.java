package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:27
 * @Description:女性接口
 */
public interface IWomen {
    //获得个人状况
    public int getType();
    //获取个人请示，你要干什么？出去逛街？约会？还是看电影？
    public String getRequest();
}
