package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

/**
 * @Auther: csh
 * @Date: 2020/6/4 17:00
 * @Description:组长
 */
public class GroupManager extends Handler {
    @Override
    protected void doHandler(Employe employe) {
        if(employe.getDays()<=2){
            System.out.println("组长,审批通过");
        }else{
            if(getNextHandler()!=null){
                getNextHandler().doHandler(employe);
            }else{
                System.out.println("没法审批！");
            }
        }
    }
}
