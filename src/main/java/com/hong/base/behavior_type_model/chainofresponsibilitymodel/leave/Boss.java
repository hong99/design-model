package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

/**
 * @Auther: csh
 * @Date: 2020/6/4 17:04
 * @Description:老板
 */
public class Boss extends Handler {
    @Override
    protected void doHandler(Employe employe) {
        if(employe.getDays()<=14){
            System.out.println("老板,审批通过！");
        }else{
            if(getNextHandler()!=null){
                getNextHandler().doHandler(employe);
            }else{
                System.out.println("这他/她妈的不干直接开掉！");
            }
        }
    }
}
