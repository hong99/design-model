package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

/**
 * @Auther: csh
 * @Date: 2020/6/4 16:55
 * @Description:
 */
public class Employe {
    //员工名称
    private String name;
    //请假天数
    private Integer days;

    public Employe(String name, Integer days) {
        this.name = name;
        this.days = days;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }
}
