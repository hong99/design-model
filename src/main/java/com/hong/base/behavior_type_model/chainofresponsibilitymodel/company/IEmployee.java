package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 11:31
 * @Description:抽象员工(抽象)
 */
public interface IEmployee {
    //请假类型
    int getType();
    //请假原因
    String getRequest();

}
