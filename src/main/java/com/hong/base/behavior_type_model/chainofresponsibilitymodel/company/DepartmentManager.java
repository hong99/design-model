package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 11:43
 * @Description:部门经理
 */
public class DepartmentManager extends Handler {
    public DepartmentManager() {
    }

    public DepartmentManager(int level) {
        super(Handler.DEPARTMENT_LEVEL_REQUEST);
    }

    @Override
    protected void response(IEmployee employee) {
        System.out.println("---下属的请示-------");
        System.out.println(employee.getRequest());
        System.out.println("部门经理回复是:同意\n");
    }
}
