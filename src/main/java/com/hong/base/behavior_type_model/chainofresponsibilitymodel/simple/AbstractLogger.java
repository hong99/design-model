package com.hong.base.behavior_type_model.chainofresponsibilitymodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/3 18:21
 * @Description:抽象记录器
 */
public abstract class AbstractLogger {
    //打印
    public static int INFO = 1;
    //调试
    public static int DEBUG = 2;
    //错误
    public static int ERROR = 3;
    //级别
    protected int level;

    //责任链中的下一个元素
    protected AbstractLogger nextLogger;

    public AbstractLogger(AbstractLogger nextLogger) {
        this.nextLogger = nextLogger;
    }

    protected AbstractLogger() {
    }

    //层级打印
    public void logMessage(int level, String message){
        if(this.level <= level){
            write(message);
        }
        if(nextLogger !=null){
            nextLogger.logMessage(level, message);
        }
    }

    abstract protected void write(String message);

    public AbstractLogger getNextLogger() {
        return nextLogger;
    }

    public void setNextLogger(AbstractLogger nextLogger) {
        this.nextLogger = nextLogger;
    }
}
