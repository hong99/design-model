package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

import java.util.ArrayList;
import java.util.Random;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:37
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        //随机挑选几个女性
        Random random = new Random();
        ArrayList<IWomen> arrayList = new ArrayList <IWomen>();
        for(int i=0;i<5;i++){
            arrayList.add(new Women(random.nextInt(4),"我要出去逛街"));
        }
        //定义三个请示对象
        Handler father = new Father();
        Handler husband = new Husband();
        Handler son = new Son();
        father.setNext(husband);
        husband.setNext(son);
        for (IWomen iWomen : arrayList) {
            father.handlerMessage(iWomen);
        }
    }
}
