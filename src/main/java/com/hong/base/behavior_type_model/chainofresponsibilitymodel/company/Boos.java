package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 11:43
 * @Description:老板
 */
public class Boos extends Handler {


    public Boos() {
    }

    public Boos(int level) {
        super(Handler.BOOS_LEVEL_REQUEST);
    }

    @Override
    protected void response(IEmployee employee) {
        System.out.println("---下属的请示-------");
        System.out.println(employee.getRequest());
        System.out.println("老板回复是:同意\n");
    }
}
