package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

/**
 * @Auther: csh
 * @Date: 2020/6/4 17:03
 * @Description:部门经理
 */
public class DeparmentManger  extends Handler{
    @Override
    protected void doHandler(Employe employe) {
        if(employe.getDays()<=5){
            System.out.println("经理，审批通过");
        }else{
            if(getNextHandler()!=null){
                getNextHandler().doHandler(employe);
            }else{
                System.out.println("无法审批!");
            }
        }
    }
}
