package com.hong.base.behavior_type_model.chainofresponsibilitymodel.leave;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/4 17:05
 * @Description:员工请假例子
 * 该例子是通过员工请假天数进行判断由哪个负责审批;
 */
public class Client {
    public static void main(String[] args) {
        Handler groupManager = new GroupManager();
        Handler deparmentManger = new DeparmentManger();
        Handler boos = new Boss();
        groupManager.setNextHandler(deparmentManger);
        deparmentManger.setNextHandler(boos);

        List<Employe>  list = new ArrayList <Employe>();
        for (int i =0;i<5;i++){
            list.add(new Employe(i+"员工",getRandom(1,14)));
        }

        for (Employe employe : list) {
            groupManager.doHandler(employe);
        }


    }

    //随机数
    public static int getRandom(int start,int end) {

        int num=(int) (Math.random()*(end-start+1)+start);
        return num;
    }
}
