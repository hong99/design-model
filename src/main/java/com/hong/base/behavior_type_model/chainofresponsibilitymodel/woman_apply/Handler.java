package com.hong.base.behavior_type_model.chainofresponsibilitymodel.woman_apply;

/**
 * @Auther: csh
 * @Date: 2020/6/4 10:43
 * @Description:抽象的类
 */
public abstract class Handler {
    public final static int FATHER_LEVEL_PEQUEST =1;
    public final static int HUSBAND_LEVEL_REQUEST=2;
    public final static int SON_LEVEL_REQUEST=3;
    //能处理的级别
    private int level = 0;
    //责任传递，一一个人责任人是谁
    private Handler nexHandler;
    //每个类都要说明一下自己能处理哪些请求


    public Handler() {
    }

    public Handler(int level) {
        this.level = level;
    }
    //一个女性（女儿、妻子或者是母亲）要求逛街、你要处理这个请求
    public final void handlerMessage(IWomen women){
        if(women.getType()==this.level){
            this.response(women);
        }else{
            if(this.nexHandler!=null){
                this.nexHandler.handlerMessage(women);
            }else{
                System.out.println("----没有地方请求了，按不同意处理------");
            }
        }
    }

    public void setNext(Handler handler){
        this.nexHandler = handler;
    }
    //有请示那当然要回应
    protected abstract void response(IWomen women);
}
