package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 14:32
 * @Description:组长
 */
public class GroupManager extends Handler {

    public GroupManager() {
    }

    public GroupManager(int level) {
        super(Handler.GROUP_LEVEL_PEQUEST);
    }

    @Override
    protected void response(IEmployee employee) {
        System.out.println("---下属的请示-------");
        System.out.println(employee.getRequest());
        System.out.println("组长回复是:同意\n");
    }
}
