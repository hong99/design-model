package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

import java.util.ArrayList;

/**
 * @Auther: csh
 * @Date: 2020/6/4 14:36
 * @Description:演示 责任链模式
 * 演示 员工申请的假由组长批、组长申请的假由部门经理批、部门经理申请的假由老板批;
 */
public class Client {
    public static void main(String[] args) {
        //随机先成几个员工
        ArrayList<IEmployee> arrayList = new ArrayList <IEmployee>();
        for(int i=0;i<10;i++){
            arrayList.add(new Employee(getRandom(1,3),"家里有事"));
        }
        //定义三个请示对象
        Handler groupManager = new GroupManager(Handler.GROUP_LEVEL_PEQUEST);
        Handler deparmentManager= new DepartmentManager(Handler.DEPARTMENT_LEVEL_REQUEST);
        Handler boos = new Boos(Handler.BOOS_LEVEL_REQUEST);
        groupManager.setNext(deparmentManager);
        deparmentManager.setNext(boos);
        for (IEmployee iEmployee : arrayList) {
            groupManager.handlerMessage(iEmployee);
        }
    }

    //随机数
    public static int getRandom(int start,int end) {

        int num=(int) (Math.random()*(end-start+1)+start);
        return num;
    }
}
