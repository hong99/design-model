package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 11:43
 * @Description:
 */
public class Employee implements IEmployee {
    /**
     *
     * 功能描述:职位类型
     *
     * 1.普通员工
     * 2.组长
     * 3.部门经理
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/6/4 11:44
     */
    private int type=1;
    //妇女的请示
    private String request= "";

    @Override
    public int getType() {
        return type;
    }

    @Override
    public String getRequest() {
        return request;
    }

    public Employee(int type, String request) {
        this.type = type;
        switch (this.type){
            case 1:
                this.request="员工的请求是:"+request;
                break;
            case 2:
                this.request="组长的请求是:"+request;
                break;
            case 3:
                this.request="经理的请求是:"+request;
                break;
        }
    }
}
