package com.hong.base.behavior_type_model.chainofresponsibilitymodel.company;

/**
 * @Auther: csh
 * @Date: 2020/6/4 14:25
 * @Description:拥有审批权的负责人(handler)
 */
public abstract class Handler {
    public final static int GROUP_LEVEL_PEQUEST =1;
    public final static int DEPARTMENT_LEVEL_REQUEST=2;
    public final static int BOOS_LEVEL_REQUEST=3;
    //能批的假级别
    private int level = 0;
    //责任传递，一个人责任人是谁
    private Handler  nexHandler;


    public Handler() {
    }

    public Handler(int level) {
        this.level = level;
    }

    //请求文本
    public final void handlerMessage(IEmployee employee){
        if(employee.getType()==this.level){
            this.response(employee);
        }else{
            if(this.nexHandler!=null){
                this.nexHandler.handlerMessage(employee);
            }else{
                System.out.println("----大boos，直接过------");
            }
        }
    }

    public void setNext(Handler handler){
        this.nexHandler = handler;
    }

    //回应批复结果
    protected abstract void response(IEmployee employee);

}
