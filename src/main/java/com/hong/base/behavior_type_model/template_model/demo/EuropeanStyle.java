package com.hong.base.behavior_type_model.template_model.demo;

/**
 * @ClassName EuropeanStyle
 * @Author csh
 * @Description 实现(Concrete Template)
 * @Date 2020/6/21
 **/
public class EuropeanStyle extends Houser {
    @Override
    void initialize() {
        System.out.println("准备欧式风格材料完毕!");
    }

    @Override
    void wall() {
        System.out.println("大理石墙面!");
    }

    @Override
    void floor() {
        System.out.println("大理石地板!");
    }
}
