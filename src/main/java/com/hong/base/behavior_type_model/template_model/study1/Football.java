package com.hong.base.behavior_type_model.template_model.study1;
/**
 *
 * 功能描述:模板实现
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/18 10:22
 */
public class Football extends Game {
 
   @Override
   void endPlay() {
      System.out.println("Football Game Finished!");
   }
 
   @Override
   void initialize() {
      System.out.println("Football Game Initialized! Start playing.");
   }
 
   @Override
   void startPlay() {
      System.out.println("Football Game Started. Enjoy the game!");
   }
}