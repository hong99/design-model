package com.hong.base.behavior_type_model.template_model.demo;

/**
 * @ClassName Client
 * @Author csh
 * @Description 模板模式，通过抽象父类子类进行继承然后进行实现。
 * @Date 2020/6/21
 **/
public class Client {
    public static void main(String[] args) {
        Houser chineseStyle = new Sinicism();
        chineseStyle.decor();
        EuropeanStyle europeanStyle = new EuropeanStyle();
        europeanStyle.decor();
    }
}
