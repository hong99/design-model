package com.hong.base.behavior_type_model.template_model.demo;

/**
 * @ClassName Houser （模板）
 * @Author  csh
 * @Description
 * @Date 2020/6/21
 **/
public abstract class Houser {
    abstract void initialize();
    abstract void wall();
    abstract void floor();

    //模板
    public final void decor(){
        //初始化房子
        initialize();
        //刷墙
        wall();
        //贴地板
        floor();
    }
}
