package com.hong.base.behavior_type_model.template_model.study1;
/**
 *
 * 功能描述:模板
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/18 10:19
 */
public abstract class Game {
   abstract void initialize();
   abstract void startPlay();
   abstract void endPlay();
 
   //模板
   public final void play(){
 
      //初始化游戏
      initialize();
 
      //开始游戏
      startPlay();
 
      //结束游戏
      endPlay();
   }
}