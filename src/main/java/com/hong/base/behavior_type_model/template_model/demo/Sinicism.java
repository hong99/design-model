package com.hong.base.behavior_type_model.template_model.demo;

/**
 * @ClassName Sinicism
 * @Author csh
 * @Description 实现(Concrete Template)
 * @Date 2020/6/21
 **/
public class Sinicism extends Houser {
    @Override
    void initialize() {
        System.out.println("中国风格基础材料准备完毕!");
    }

    @Override
    void wall() {
        System.out.println("皇宫墙");
    }

    @Override
    void floor() {
        System.out.println("木板房子");
    }
}
