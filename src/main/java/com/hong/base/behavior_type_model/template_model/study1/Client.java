package com.hong.base.behavior_type_model.template_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/18 10:22
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Game game = new Cricket();
        game.play();
        game  = new Football();
        game.play();
    }
}
