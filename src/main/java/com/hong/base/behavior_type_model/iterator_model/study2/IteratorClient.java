package com.hong.base.behavior_type_model.iterator_model.study2;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/9 09:49
 * @Description:将list放到容器中，然后再遍历。
 */
public class IteratorClient {
    private List list;
    public void setContainer(List list){
        this.list = list;
    }

    //遍历容器的数据
    public void printList(){
        if(null==list){
            throw new NullPointerException();
        }

        for (Object obj : list) {
            System.out.print(obj);
        }
    }

    public static void main(String[] args) {

        IteratorClient it = new IteratorClient();


        List list = new ArrayList();
        for(int i=0;i<10;i++){
            list.add(i);
        }

        it.setContainer(list);

        it.printList();

    }
}
