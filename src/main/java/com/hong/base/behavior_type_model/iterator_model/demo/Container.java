package com.hong.base.behavior_type_model.iterator_model.demo;

import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/9 10:18
 * @Description:数据容器接口(Container)
 */
public abstract class Container {
    //容器数据
    public  abstract void setContainer(List list);
}
