package com.hong.base.behavior_type_model.iterator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/8 16:14
 * @Description:
 */
public class Client {

    public static void main(String[] args) {
        NameRepository nameRepository = new NameRepository();
        for(Iterator iterator =nameRepository.getIterator();iterator.hasNext();){
            String name = (String)iterator.next();
            System.out.println("name:"+name);
        }
    }
}
