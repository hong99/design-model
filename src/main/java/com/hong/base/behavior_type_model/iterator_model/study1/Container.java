package com.hong.base.behavior_type_model.iterator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/8 16:10
 * @Description:容器接口
 */
public interface Container {

    public Iterator getIterator();
}
