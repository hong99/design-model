package com.hong.base.behavior_type_model.iterator_model.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/9 10:21
 * @Description:迭代实现(ConcreteIterator)
 */
public class IteratorImpl extends Container implements Iterator {
    //聚合对象(Aggregate)
    private List list =new ArrayList();


    @Override
    public void printElements() {
        if(null!=list){
            for (Object o : list) {
                System.out.println(o);
            }
        }
    }

    @Override
    public void setContainer(List list) {
        this.list =list;
    }
}
