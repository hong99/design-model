package com.hong.base.behavior_type_model.iterator_model.demo;

/**
 * @Auther: csh
 * @Date: 2020/6/9 10:17
 * @Description:迭代抽象接口（Iterator）
 */
public interface Iterator {
    //遍历所有元素
    void printElements();
}
