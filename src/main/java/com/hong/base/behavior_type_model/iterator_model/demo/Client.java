package com.hong.base.behavior_type_model.iterator_model.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/6/9 10:29
 * @Description:班级点名
 */
public class Client {
    public static void main(String[] args) {
        //具体的对象 ConcreteAggregate
        String concreteAggregate =  "小明，小红，小芳，小丽，小娟，小军，小刚，小强，小方，晓明，晓红，晓芳，晓丽，晓娟，晓军，晓刚，晓强，晓方";
        String[] arr = concreteAggregate.split("，");
        List list = new ArrayList(Arrays.asList(arr));
        IteratorImpl iterator = new IteratorImpl();
        iterator.setContainer(list);
        iterator.printElements();
    }
}
