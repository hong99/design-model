package com.hong.base.behavior_type_model.iterator_model.study1;

/**
 * @Auther: csh
 * @Date: 2020/6/8 16:07
 * @Description:迭代接口
 */
public interface Iterator {
    //是否有下一个对象
    public boolean hasNext();
    //下一个对象
    public Object next();
}
