package com.hong.base.behavior_type_model.command_model.simple;


/**
 * @Auther: csh
 * @Date: 2020/6/5 10:38
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        Stock abcStock = new Stock();

        BuyStock buyStockOrder = new BuyStock(abcStock);
        SellStock sellStockOrder = new SellStock(abcStock);

        Broker broker = new Broker();
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(sellStockOrder);

        broker.placeOrders();
    }

}
