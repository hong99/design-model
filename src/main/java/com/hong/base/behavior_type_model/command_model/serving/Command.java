package com.hong.base.behavior_type_model.command_model.serving;

/**
 * @Auther: csh
 * @Date: 2020/6/5 11:16
 * @Description:抽象命令
 */
public abstract class Command {
    protected CookReceiver cookReceiver;

    public Command(CookReceiver cookReceiver) {
        this.cookReceiver = cookReceiver;
    }

    public abstract void execute();
}
