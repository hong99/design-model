package com.hong.base.behavior_type_model.command_model.serving;

/**
 * @Auther: csh
 * @Date: 2020/6/5 11:20
 * @Description:具体命令(ConcreteCommand)
 */
public class BackBeefCommand extends Command {

    public BackBeefCommand(CookReceiver cookReceiver) {
        super(cookReceiver);
    }

    @Override
    public void execute() {
        this.cookReceiver.bakeBeef();
    }
}
