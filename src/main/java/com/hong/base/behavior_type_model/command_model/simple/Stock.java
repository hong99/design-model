package com.hong.base.behavior_type_model.command_model.simple;
/**
 *
 * 功能描述:请求类(Receiver)
 *
 * @param: 
 * @return: 
 * @auther: csh
 * @date: 2020/6/5 10:35
 */
public class Stock {
   
   private String name = "ABC";
   private int quantity = 10;
   public void buy(){
      System.out.println("Stock [ Name: "+name+", Quantity: " + quantity +" ] bought");
   }
   public void sell(){
      System.out.println("Stock [ Name: "+name+", Quantity: " + quantity +" ] sold");
   }
}