package com.hong.base.behavior_type_model.command_model.serving;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @Auther: csh
 * @Date: 2020/6/5 11:23
 * @Description:服务员 请求的发起者(Invoker)
 */
public class Waiter {
    //命令队列
    private Queue<Command>  queue = new LinkedList <Command>();

    /**
     * 添加命令
     * @param command
     */
    public void addCommand(Command command){
        if(null!=command){
            queue.add(command);
        }
    }

    /**
     * 撤销命令
     * @param command
     */
    public void cancel(Command command){
        if(!queue.isEmpty()){
            queue.remove(command);
        }
    }

    /**
     * 通知执行所有命令
     */
    public void notifyExecute() {
        while (!queue.isEmpty()) {
            Command command = queue.poll();
            command.execute();
        }
    }

}
