package com.hong.base.behavior_type_model.command_model.serving;

/**
 * @Auther: csh
 * @Date: 2020/6/5 11:28
 * @Description:命令模式测试
 */
public class Client {
    public static void main(String[] args) {
        //新店开张
        Waiter waiter = new Waiter();
        CookReceiver cookReceiver = new CookReceiver();
        Command backbeefCommand =new BackBeefCommand(cookReceiver);
        Command backMuttonCommand = new BackMuttonCommand(cookReceiver);

        //接单
        waiter.addCommand(backbeefCommand);
        waiter.addCommand(backMuttonCommand);

        //顾客一些不要了
        waiter.cancel(backbeefCommand);

        //通知厨师制作
        waiter.notifyExecute();

    }
}
