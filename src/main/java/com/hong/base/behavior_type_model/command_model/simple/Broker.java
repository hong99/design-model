package com.hong.base.behavior_type_model.command_model.simple;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * 功能描述:调用者(Invoker)
 *
 * @param: 
 * @return: 
 * @auther: csh
 * @date: 2020/6/5 10:37
 */
public class Broker {
   private List<IOrder> orderList = new ArrayList<IOrder>();
 
   public void takeOrder(IOrder order){
      orderList.add(order);      
   }
 
   public void placeOrders(){
      for (IOrder order : orderList) {
         order.execute();
      }
      orderList.clear();
   }
}