package com.hong.base.behavior_type_model.command_model.serving;

/**
 * @Auther: csh
 * @Date: 2020/6/5 11:14
 * @Description:厨师
 */
public class CookReceiver {

    public void bakeBeef(){
        System.out.println("厨师：烤牛肉");
    }

    public void bakeMutton(){
        System.out.println("厨师：烤羊肉");
    }
}
