package com.hong.base.behavior_type_model.command_model.simple;
/**
 *
 * 功能描述:命令实现
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/5 10:36
 */
public class BuyStock implements IOrder {
   private Stock abcStock;
 
   public BuyStock(Stock abcStock){
      this.abcStock = abcStock;
   }
 
   @Override
   public void execute() {
      abcStock.buy();
   }
}