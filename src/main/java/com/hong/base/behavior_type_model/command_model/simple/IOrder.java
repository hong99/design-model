package com.hong.base.behavior_type_model.command_model.simple;


/**
 * @Auther: csh
 * @Date: 2020/6/5 10:27
 * @Description:命令接口
 */
public interface IOrder {
    void execute();
}
