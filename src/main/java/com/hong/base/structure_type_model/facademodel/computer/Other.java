package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:23
 * @Description:其他组件
 */
public class Other implements IModule {
    @Override
    public void installModule() {
        System.out.println("安装其他组件 键盘、鼠标、硬盘等");
    }
}
