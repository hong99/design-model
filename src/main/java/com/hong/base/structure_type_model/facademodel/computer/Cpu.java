package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:21
 * @Description:CPU
 */
public class Cpu implements IModule {
    @Override
    public void installModule() {
        System.out.println("安装CPU");
    }
}
