package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:20
 * @Description:抽象组件
 */
public interface IModule {
    //安装组件
    void installModule();
}
