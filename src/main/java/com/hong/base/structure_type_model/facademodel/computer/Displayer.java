package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:22
 * @Description:显示器
 */
public class Displayer implements IModule {
    @Override
    public void installModule() {
        System.out.println("安装显示器");
    }
}
