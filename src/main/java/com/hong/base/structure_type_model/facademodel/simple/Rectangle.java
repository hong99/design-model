package com.hong.base.structure_type_model.facademodel.simple;
/**
 *
 * 功能描述: 长方形
 *
 * @param: 
 * @return: 
 * @auther: csh
 * @date: 2020/5/29 11:16
 */
public class Rectangle implements Shape {
 
   @Override
   public void draw() {
      System.out.println("长方形");
   }
}