package com.hong.base.structure_type_model.facademodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:16
 * @Description:形状
 */
public interface Shape {
    void draw();
}
