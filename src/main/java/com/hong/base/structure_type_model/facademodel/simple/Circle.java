package com.hong.base.structure_type_model.facademodel.simple;
/**
 *
 * 功能描述:圆形
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/29 11:17
 */
public class Circle implements Shape {
 
   @Override
   public void draw() {
      System.out.println("圆形");
   }
}