package com.hong.base.structure_type_model.facademodel.simple;
/**
 *
 * 功能描述: 外观类
 *
 * @param:
 * @return: 
 * @auther: csh
 * @date: 2020/5/29 11:18
 */
public class ShapeMaker {
   private Shape circle;
   private Shape rectangle;
   private Shape square;
 
   public ShapeMaker() {
      circle = new Circle();
      rectangle = new Rectangle();
      square = new Square();
   }
 
   public void drawCircle(){
      circle.draw();
   }
   public void drawRectangle(){
      rectangle.draw();
   }
   public void drawSquare(){
      square.draw();
   }
}