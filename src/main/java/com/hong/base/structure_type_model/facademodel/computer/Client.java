package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:26
 * @Description:安装电脑
 */
public class Client {
    public static void main(String[] args) {
        ComputerMarker computerMarker = new ComputerMarker();
        computerMarker.installCput();
        computerMarker.installDisplayer();
        computerMarker.installMemoryBank();
        computerMarker.installOther();
        System.out.println("电脑装好了直接拿回家！");
    }
}
