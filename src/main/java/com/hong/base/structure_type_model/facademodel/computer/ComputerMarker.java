package com.hong.base.structure_type_model.facademodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/29 11:23
 * @Description:构建者
 */
public class ComputerMarker  {
    private Cpu cpu;
    private Displayer displayer;
    private MemoryBank memoryBank;
    private Other other;

    public ComputerMarker() {
        this.cpu = new Cpu();
        this.displayer = new Displayer();
        this.memoryBank = new MemoryBank();
        this.other = new Other();
    }

    public void installCput(){
        cpu.installModule();
    }

    public void installDisplayer(){
        displayer.installModule();
    }

    public void installMemoryBank(){
        memoryBank.installModule();
    }

    public void installOther(){
        other.installModule();
    }

}
