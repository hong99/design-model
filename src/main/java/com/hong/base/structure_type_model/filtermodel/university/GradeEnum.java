package com.hong.base.structure_type_model.filtermodel.university;

/**
 * @Auther: csh
 * @Date: 2020/5/25 14:26
 * @Description:分数枚举
 */
public enum GradeEnum  {
    ONE_COLLEGE(700,"1本"),
    TWO_COLLEGE(600,"2本"),
    THREE_COLLEGE(400,"3本"),
    COMMON_COLLEGE(200,"普通专科")
    ;

    private int grade;
    private String name;

    GradeEnum(int grade, String name) {
        this.grade = grade;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
