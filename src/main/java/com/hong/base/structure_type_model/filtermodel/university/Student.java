package com.hong.base.structure_type_model.filtermodel.university;

import java.io.Serializable;

/**
 * @Auther: csh
 * @Date: 2020/5/25 14:10
 * @Description:学生
 */
public class Student implements Serializable {
    /** 人名*/
    private String realName;
    /** 分数*/
    private int grade;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Student(String realName, int grade) {
        this.realName = realName;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "realName='" + realName + '\'' +
                ", grade=" + grade +
                '}';
    }
}
