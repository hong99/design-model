package com.hong.base.structure_type_model.filtermodel.university;

import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 14:16
 * @Description:分数拦截器
 */
public interface IGradeFilter {
    /** 分数拦截器*/
    List<Student> conditionGradeFilter(List<Student> studentList);
}
