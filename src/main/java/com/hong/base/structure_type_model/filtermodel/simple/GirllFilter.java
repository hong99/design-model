package com.hong.base.structure_type_model.filtermodel.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 11:22
 * @Description:女生过滤器
 */
public class GirllFilter implements IFliter {
    @Override
    public List<Person> filter(List <Person> personList) {
        List <Person> girlList = new ArrayList <Person>();
        for (Person person : personList) {
            if(person.getSex().equals(SexEnum.GIRL.getSex())){
                girlList.add(person);
            }
        }
        return girlList;
    }
}
