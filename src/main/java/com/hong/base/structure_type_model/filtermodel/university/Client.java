package com.hong.base.structure_type_model.filtermodel.university;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 14:34
 * @Description:演示
 */
public class Client  {

    public static void main(String[] args) {
        List<Student> students = new ArrayList <Student>();
        for(int i =0;i<20;i++){
            students.add(new Student(i+"学生",getRandom(200,750)));
        }
        for (Student student : students) {
            System.out.println(student);
        }
        System.out.println(students.size()+"个学生！");
        System.out.println("----------------------------------------");
        BestUniversityFilter best = new BestUniversityFilter();
        List <Student> bestList = best.conditionGradeFilter(students);
        for (Student student : bestList) {
            System.out.println(student);
        }
        System.out.println(bestList.size()+"个"+GradeEnum.ONE_COLLEGE.getName());
        System.out.println("-------------------------------------------");
        SecondUniversityFilter second = new SecondUniversityFilter();
        List <Student> secondList = second.conditionGradeFilter(students);
        for (Student student : secondList) {
            System.out.println(student);
        }
        System.out.println(secondList.size()+"个"+GradeEnum.TWO_COLLEGE.getName());
        System.out.println("-------------------------------------------");
        ThreeUniversityFilter threeFilter = new ThreeUniversityFilter();
        List <Student> threeeList = threeFilter.conditionGradeFilter(students);
        for (Student student : threeeList) {
            System.out.println(student);
        }
        System.out.println(threeeList.size()+"个"+GradeEnum.THREE_COLLEGE.getName());
        System.out.println("-------------------------------------------");
        ThreeUniversityFilter commonFilter = new ThreeUniversityFilter();
        List <Student> commonList = commonFilter.conditionGradeFilter(students);
        for (Student student : commonList) {
            System.out.println(student);
        }
        System.out.println(threeeList.size()+"个"+GradeEnum.COMMON_COLLEGE.getName());
    }

    //随机数
    public static int getRandom(int start,int end) {

        int num=(int) (Math.random()*(end-start+1)+start);
        return num;
    }
}
