package com.hong.base.structure_type_model.filtermodel.simple;

import java.io.Serializable;

/**
 * @Auther: csh
 * @Date: 2020/5/25 11:19
 * @Description:人
 */
public class Person implements Serializable {
    /** 姓名*/
    private String name;
    /** 姓别*/
    private String sex;

    public Person(String name, String sex) {
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
