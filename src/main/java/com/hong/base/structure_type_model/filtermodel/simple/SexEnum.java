package com.hong.base.structure_type_model.filtermodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/25 11:22
 * @Description:
 */
public enum SexEnum {

    GIRL("女"),
    BOY("男");

    private String sex;

    SexEnum(String age) {
        this.sex = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
