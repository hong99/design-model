package com.hong.base.structure_type_model.filtermodel.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 11:29
 * @Description:过滤测试
 */
public class Client {

    public static void main(String[] args) {
        List<Person>  personList = new ArrayList <Person>();
        for(int i =1;i<=10;i++){
            if(i<=4){
                personList.add(new Person(i+":先生",SexEnum.BOY.getSex()));
            }else{
                personList.add(new Person(i+":女士",SexEnum.GIRL.getSex()));
            }
        }

        GirllFilter girllFilter = new GirllFilter();
        List <Person> girlList = girllFilter.filter(personList);
        for (Person person : girlList) {
            System.out.println(person);
        }
        System.out.println("总共有:"+girlList.size()+"位女生");
        BoyFilter boyFilter = new BoyFilter();
        List <Person> boyList = boyFilter.filter(personList);
        for (Person person : boyList) {
            System.out.println(person);
        }
        System.out.println("总共有:"+boyList.size()+"位先生");

    }
}
