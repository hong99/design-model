package com.hong.base.structure_type_model.filtermodel.university;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 14:30
 * @Description:三本大学
 */
public class ThreeUniversityFilter implements  IGradeFilter{
    @Override
    public List<Student> conditionGradeFilter(List <Student> studentList) {
        List<Student> list = new ArrayList <Student>();
        for (Student student : studentList) {
            if(student.getGrade()>=GradeEnum.THREE_COLLEGE.getGrade() && student.getGrade()<GradeEnum.TWO_COLLEGE.getGrade()){
                list.add(student);
            }
        }
        return list;
    }
}
