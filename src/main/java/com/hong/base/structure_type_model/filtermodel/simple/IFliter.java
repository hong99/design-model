package com.hong.base.structure_type_model.filtermodel.simple;

import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 11:20
 * @Description:过滤器接口
 */
public interface IFliter  {
    /** 过滤接口*/
    List<Person> filter(List<Person> personList);
}
