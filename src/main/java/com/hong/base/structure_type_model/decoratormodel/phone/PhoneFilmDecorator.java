package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:10
 * @Description:贴膜装饰器
 */
public class PhoneFilmDecorator extends IPhoneDecorator {
    public PhoneFilmDecorator(IPhone phone) {
        super(phone);
    }



    @Override
    public void callUp(String number) {
        phone.callUp(number);
    }

    @Override
    public void internet(int newWork) {
        phone.internet(newWork);
    }

    @Override
    public void printFunction() {
        super.printFunction();
        this.setFilm();
    }

    private void setFilm(){
        System.out.println("贴了膜");
    }
}
