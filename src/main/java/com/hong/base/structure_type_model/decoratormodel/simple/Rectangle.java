package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:41
 * @Description:实现接口
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("矩形");

    }
}
