package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:07
 * @Description:抽象的修饰器
 */
public abstract class IPhoneDecorator implements IPhone  {
    protected IPhone phone;

    public IPhoneDecorator(IPhone phone) {
        this.phone = phone;
    }

    @Override
    public void printFunction() {
        phone.printFunction();
    }
}

