package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:02
 * @Description:抽象的手机
 */
public interface IPhone {
    //打电话
    void callUp(String number);
    //上网
    void internet(int newWork);
    //基本功能
    void printFunction();
}
