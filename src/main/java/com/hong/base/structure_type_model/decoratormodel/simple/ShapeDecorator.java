package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:43
 * @Description:抽象装饰类
 */
public abstract class ShapeDecorator implements Shape{
    protected Shape decoratedShape;

    public ShapeDecorator(Shape decoratedShape) {
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void draw() {
        decoratedShape.draw();
    }
}
