package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:53
 * @Description:修饰类
 */
public class Client {
    public static void main(String[] args) {
        Shape circle = new Cirle();
        ShapeDecorator redCrile = new RedShapeDecorator(new Cirle());
        ShapeDecorator twoRedBorder = new RedShapeDecorator(new Rectangle());
        System.out.println("这个图型没有框");
        circle.draw();

        System.out.println("这个图型有红框");
        redCrile.draw();

        System.out.println("这个图型也有红色框");
        twoRedBorder.draw();

    }
}
