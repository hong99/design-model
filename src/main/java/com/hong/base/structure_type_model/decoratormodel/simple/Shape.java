package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:41
 * @Description:抽象型状
 */
public interface Shape {
    void draw();
}
