package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:42
 * @Description:圆形
 */
public class Cirle implements Shape {
    @Override
    public void draw() {
        System.out.println("圆形");
    }
}
