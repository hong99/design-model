package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:06
 * @Description:小米手机
 */
public class XMPhone implements IPhone {
    @Override
    public void callUp(String number) {
        System.out.println("小米手机打电话:"+number);
    }

    @Override
    public void internet(int newWork) {
        System.out.println("上的网络"+newWork+"G");

    }

    @Override
    public void printFunction() {
        System.out.println("小米手机有手电话和上网功能");

    }
}
