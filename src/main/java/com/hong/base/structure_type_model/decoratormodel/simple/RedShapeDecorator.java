package com.hong.base.structure_type_model.decoratormodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/5/28 16:51
 * @Description:实体修饰类
 */
public class RedShapeDecorator extends ShapeDecorator{

    public RedShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        setRedBorder(decoratedShape);
    }

    private void setRedBorder(Shape decoratedShape){
        System.out.println("红色的框");
    }
}
