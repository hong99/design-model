package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:10
 * @Description:手机壳装饰器
 */
public class PhoneShellDecorator extends IPhoneDecorator {
    public PhoneShellDecorator(IPhone phone) {
        super(phone);
    }



    @Override
    public void callUp(String number) {
        phone.callUp(number);
    }

    @Override
    public void internet(int newWork) {
        phone.internet(newWork);
    }

    @Override
    public void printFunction() {
        super.printFunction();
        this.setShell();
    }

    private void setShell(){
        System.out.println("加装了手机壳");
    }
}
