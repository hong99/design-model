package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:05
 * @Description:华为手机
 */
public class HWPhone implements IPhone {
    @Override
    public void callUp(String number) {
        System.out.println("正在打电话:"+number);

    }

    @Override
    public void internet(int newWork) {
        System.out.println("上的网络"+newWork+"G");
    }

    @Override
    public void printFunction() {
        System.out.println("华为手机有手电话和上网功能");
    }
}
