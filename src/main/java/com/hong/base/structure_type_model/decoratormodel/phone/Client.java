package com.hong.base.structure_type_model.decoratormodel.phone;

/**
 * @Auther: csh
 * @Date: 2020/5/28 18:16
 * @Description:案例
 */
public class Client {

    public static void main(String[] args) {
        HWPhone hwPhone  = new HWPhone();
        hwPhone.printFunction();
        PhoneFilmDecorator phoneFilmDecorator = new PhoneFilmDecorator(hwPhone);
        XMPhone xmphone = new XMPhone();
        phoneFilmDecorator.printFunction();
        PhoneShellDecorator phoneShellDecorator = new PhoneShellDecorator(xmphone);
        phoneShellDecorator.printFunction();
    }
}
