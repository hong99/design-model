package com.hong.base.structure_type_model.proxymodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:21
 * @Description:演示
 */
public class Client {

    public static void main(String[] args) {
        IImage image = new ProxyImage("123.jpg");
        //图像将从磁盘加载
        image.display();
        System.out.println("");
        //图像不需要从磁盘加载
        image.display();
    }
}
