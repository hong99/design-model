package com.hong.base.structure_type_model.proxymodel.car.static_proxy;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:39
 * @Description:代驾列子(静态代理)
 */
public class Client {

    public static void main(String[] args) {
        ICar car =new  CarProxy("滴滴代驾");
        car.drive();
    }
}
