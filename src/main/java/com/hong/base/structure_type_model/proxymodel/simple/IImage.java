package com.hong.base.structure_type_model.proxymodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/2 10:44
 * @Description:图片接口
 */
public interface IImage {
    void display();
}
