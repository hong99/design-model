package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.cglib;

/**
 * @Auther: csh
 * @Date: 2020/6/2 17:37
 * @Description:cglib动态代理
 * 区别于jdk代理在于
 * 1.CGLIB可代理类，可JDK代理只能代理方法
 * 2.CGLIB可以运行时动态增加类或方法，而JDK代理不能;
 * 3.CGLIB不能代理final的方法，并且比JDK代理快;
 */
public class Client {
    public static void main(String[] args) {
        BMWCar car = (BMWCar)ProxyFacotory.getcglibProxy(new BMWCar());
        car.setName("滴滴代驾");
        car.drive();

    }
}
