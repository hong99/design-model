package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Auther: csh
 * @Date: 2020/6/2 17:31
 * @Description:CGLIB拦截器
 * 在调用目标方法时，CGLib会回调MethodInterceptor接口方法拦截，来实现你自己的代理逻辑，类似于JDK中的InvocationHandler接口
 */
public class CglibProxy implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("被拦截");
        Object invoke = methodProxy.invokeSuper(o, objects);
        return invoke;
    }
}
