package com.hong.base.structure_type_model.proxymodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:15
 * @Description:读图片
 */
public class RealImage implements IImage {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        System.out.println("Displaying"+fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading"+fileName);
    }
}
