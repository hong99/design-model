package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @Auther: csh
 * @Date: 2020/6/2 16:25
 * @Description:jdk动态代理
 */
public class Client {
    public static void main(String[] args) {
        //代驾
        BMWCar car = new BMWCar("滴滴代驾");
        InvocationHandler proHandler = new ProHandler(car);
        ICar bmwCar = (ICar) Proxy.newProxyInstance(BMWCar.class.getClassLoader(), BMWCar.class.getInterfaces(), proHandler);
        bmwCar.drive();
    }
}
