package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.jdk;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:35
 * @Description:抽象车(subject)
 */
public interface ICar {
    //开车
    void drive();
}
