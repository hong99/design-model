package com.hong.base.structure_type_model.proxymodel.car.static_proxy;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:38
 * @Description:代驾(Proxy)
 */
public class CarProxy implements ICar {
    //宝马
    private BMWCar bmwCar;
    //代驾名称
    private String proxyName;

    public CarProxy(String proxyName) {
        this.proxyName = proxyName;
    }

    @Override
    public void drive() {
        if(null==bmwCar){
            bmwCar = new BMWCar(proxyName);
        }
        bmwCar.drive();
    }
}
