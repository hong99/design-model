package com.hong.base.structure_type_model.proxymodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:20
 * @Description:
 */
public class ProxyImage implements IImage {

    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage==null){
            realImage =new RealImage(fileName);
        }
        realImage.display();
    }
}
