package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.jdk;

/**
 * @Auther: csh
 * @Date: 2020/6/2 11:36
 * @Description:宝马车(RealSubject)
 */
public class BMWCar implements ICar {
    //开车人名称
    private String name;

    public BMWCar(String name) {
        this.name = name;
    }

    @Override
    public void drive() {
        System.out.println("驾驶宝马的人："+name);
    }
}
