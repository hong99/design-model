package com.hong.base.structure_type_model.proxymodel.car.dynamic_proxy.cglib;

import net.sf.cglib.proxy.Enhancer;

/**
 * @Auther: csh
 * @Date: 2020/6/2 17:35
 * @Description:代理工厂
 */
public class ProxyFacotory {
    public static Object getcglibProxy(Object target){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(new CglibProxy());
        Object targetProxy = enhancer.create();
        return targetProxy;
    }
}
