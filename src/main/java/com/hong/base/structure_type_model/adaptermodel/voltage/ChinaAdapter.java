package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 17:01
 * @Description:中国适配器
 */
public class ChinaAdapter extends Voltage220V implements IOutVoltage5V, IOutVoltage12V {
    int voltage12V = super.outPutVoltage();
    int voltage5V = super.outPutVoltage();
    @Override
    public int outVoltage12V() {
        System.out.println("将中国的220V转成12V...");
        voltage12V = 12;
        return voltage12V;
    }

    @Override
    public int outVoltage5V() {
        System.out.println("将中国的220V转成5V...");
        voltage5V = 5;
        return voltage5V;
    }
}
