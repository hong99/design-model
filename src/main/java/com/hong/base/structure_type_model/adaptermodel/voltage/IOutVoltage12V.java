package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 16:59
 * @Description:12V接口
 */
public interface IOutVoltage12V {
    int outVoltage12V();
}
