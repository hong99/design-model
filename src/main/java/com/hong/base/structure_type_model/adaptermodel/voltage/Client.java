package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 15:08
 * @Description:适配器
 */
public class Client {

    public static void main(String[] args) {
        //中国转换器
        //手机充电
        Phone phone = new Phone();
        ChinaAdapter chinaAdapter = new ChinaAdapter();
        phone.charging(chinaAdapter);
        //电脑充电
        Computer computer = new Computer();
        computer.charging(chinaAdapter);
        //美国转换器
        //手机充电
        Phone phone2 = new Phone();
        ChinaAdapter chinaAdapter2 = new ChinaAdapter();
        phone2.charging(chinaAdapter2);
        //电脑充电
        Computer computer2 = new Computer();
        computer2.charging(chinaAdapter2);
        //可以自己再拓展一个整合各个国家的转换器...

    }
}
