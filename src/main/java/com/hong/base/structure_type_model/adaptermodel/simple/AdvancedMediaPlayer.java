package com.hong.base.structure_type_model.adaptermodel.simple;
/**
 *
 * 功能描述:播放接口
 *
 * @param: 
 * @return: 
 * @auther: csh
 * @date: 2020/5/19 16:20
 */
public interface AdvancedMediaPlayer { 
   //视频播放
   public void playVlc(String fileName);
   //mp4格式播放
   public void playMp4(String fileName);
}