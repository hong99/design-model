package com.hong.base.structure_type_model.adaptermodel.simple;
/**
 *
 * 功能描述:视频播放
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/19 16:21
 */
public class VlcPlayer implements AdvancedMediaPlayer{
   @Override
   public void playVlc(String fileName) {
      System.out.println("Playing vlc file. Name: "+ fileName);      
   }
 
   @Override
   public void playMp4(String fileName) {
      //什么也不做
   }
}