package com.hong.base.structure_type_model.adaptermodel.simple;
/**
 *
 * 功能描述:mp4播放
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/19 16:21
 */
public class Mp4Player implements AdvancedMediaPlayer{
 
   @Override
   public void playVlc(String fileName) {
      //什么也不做
   }
 
   @Override
   public void playMp4(String fileName) {
      System.out.println("Playing mp4 file. Name: "+ fileName);      
   }
}