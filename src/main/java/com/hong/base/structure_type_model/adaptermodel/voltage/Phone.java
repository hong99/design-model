package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 17:06
 * @Description:手机用5V电压
 */
public class Phone  {

    private int  voltage5v=5;

    void charging(IOutVoltage5V voltage5V){
        //判断用何种充电
        int i = voltage5V.outVoltage5V();
        if(voltage5v == i){
            System.out.println("手机充电中....");
        }else{
            System.out.println("电压不一致......");
        }
    }
}
