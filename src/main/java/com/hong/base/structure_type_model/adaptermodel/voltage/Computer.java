package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 17:10
 * @Description:
 */
public class Computer {
    private int  voltage12v=12;

    void charging(IOutVoltage12V voltage12V){
        //判断用何种充电
        int i = voltage12V.outVoltage12V();
        if(voltage12v == i){
            System.out.println("电脑正在充电中....");
        }else{
            System.out.println("电压不一致......");
        }
    }
}
