package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/20 17:17
 * @Description:抽象输出接口
 */
public interface IVoltage {
    public int outPutVoltage();
}
