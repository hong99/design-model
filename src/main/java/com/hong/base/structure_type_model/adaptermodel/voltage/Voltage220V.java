package com.hong.base.structure_type_model.adaptermodel.voltage;

/**
 * @Auther: csh
 * @Date: 2020/5/19 17:35
 * @Description:220V电压
 */
public class Voltage220V  implements IVoltage{
    private final static int VOLTAGE = 220;

    @Override
    public int outPutVoltage() {
        return VOLTAGE;
    }
}
