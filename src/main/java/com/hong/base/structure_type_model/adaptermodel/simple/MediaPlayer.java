package com.hong.base.structure_type_model.adaptermodel.simple;
/**
 *
 * 功能描述:播放器抽象接口
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/19 16:20
 */
public interface MediaPlayer {
   public void play(String audioType, String fileName);
}