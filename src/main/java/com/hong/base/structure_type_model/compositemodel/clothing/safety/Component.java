package com.hong.base.structure_type_model.compositemodel.clothing.safety;

/**
 * @Auther: csh
 * @Date: 2020/5/25 17:57
 * @Description:抽象构件角色类
 */
public interface Component  {
    /** 输出组件自身的名称*/
    public void printStruct(String preStr);
}
