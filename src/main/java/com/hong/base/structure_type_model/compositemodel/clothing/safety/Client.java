package com.hong.base.structure_type_model.compositemodel.clothing.safety;
/**
 *
 * 功能描述:该例为安全模式
 * 通过服务类别来区分男装和女装及子分类
 * 树枝构件类Composite给出了add()、remove()、以及getChild()等方法的声明和实现，而树叶构件类则没有给出这些方法的声明或实现。这样的做法是安全的做法，由于这个特点，客户端应用程序不可能错误的调用树叶构件的聚集方法，因为树叶构件没有这些方法，调用会导致编译错误。
 *
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/25 18:01
 */
public class Client {
    public static void main(String[] args) {
        Composite root = new Composite("服装");
        Composite c1 = new Composite("男装");
        Composite c2 = new Composite("女装");
        
        Leaf leaf1 = new Leaf("衬衫");
        Leaf leaf2 = new Leaf("夹克");
        Leaf leaf3 = new Leaf("裙子");
        Leaf leaf4 = new Leaf("套装");
        
        root.addChild(c1);
        root.addChild(c2);
        c1.addChild(leaf1);
        c1.addChild(leaf2);
        c2.addChild(leaf3);
        c2.addChild(leaf4);
        
        root.printStruct("");
    }
}