package com.hong.base.structure_type_model.compositemodel.earth.lucency;


import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 18:13
 * @Description:抽象的地方(组件)
 */
public abstract class IPosition  {

    /**
     *
     * 功能描述:打印位置名称
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:58
     */
    public abstract void printPositionName(String preStr);
    /**
     *
     * 功能描述: 添加子类
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/26 9:38
     */
    public  void addChild(IPosition child){
        throw  new UnsupportedOperationException("对象不支持此功能");
    }
    /**
     *
     * 功能描述:删除子类
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/26 9:38
     */
    public  void removeChild(IPosition child){
        throw  new UnsupportedOperationException("对象不支持此功能");
    }
    /**
     *
     * 功能描述:获取所有子类
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/26 9:40
     */
    public List<IPosition> getChild(){
        throw  new UnsupportedOperationException("对象不支持此功能");
    }


}
