package com.hong.base.structure_type_model.compositemodel.earth.safety;

/**
 * @Auther: csh
 * @Date: 2020/5/25 18:36
 * @Description:演示  （安全模式）
 */
public class Client {
    public static void main(String[] args) {
        Continent earth = new Continent("地球");
        Continent asian = new Continent("亚洲");
        Continent america = new Continent("美洲");
        Continent africa = new Continent("非洲");


        Country china = new Country("中国");
        asian.addChild(china);
        Country american = new Country("美国");
        america.addChild(american);
        Country cameroon = new Country("喀麦隆");
        africa.addChild(cameroon);

        earth.addChild(asian);
        earth.addChild(america);
        earth.addChild(africa);

        earth.printPositionName("");

    }
}
