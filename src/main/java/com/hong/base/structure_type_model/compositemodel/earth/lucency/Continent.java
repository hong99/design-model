package com.hong.base.structure_type_model.compositemodel.earth.lucency;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/26 09:41
 * @Description:洲
 */
public class Continent extends IPosition {

    /**
     *
     * 功能描述:子类集合
     *
     * @param: 
     * @return: 
     * @auther: csh
     * @date: 2020/5/26 9:42
     */
    private List<IPosition> childList = new ArrayList <IPosition>();

    /**
     *
     * 功能描述:对象名称
     *
     * @param: 
     * @return: 
     * @auther: csh
     * @date: 2020/5/26 9:42
     */
    private String name;

    @Override
    public void printPositionName(String preStr) {
        //当前的位置
        System.out.println(preStr+"+"+this.name);
        //获取包含的地区
        if(null!=childList && childList.size()>0){
            preStr+=" ";
            for (IPosition iPosition : childList) {
                iPosition.printPositionName(preStr);
            }
        }
    }


    public Continent(String name) {
        this.name = name;
    }

    @Override
    public void removeChild(IPosition child) {
        childList.remove(child);
    }

    @Override
    public void addChild(IPosition child){
        childList.add(child);
    }

    @Override
    public List <IPosition> getChild() {
        return childList;
    }
}
