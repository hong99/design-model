package com.hong.base.structure_type_model.compositemodel.earth.safety;

/**
 * @Auther: csh
 * @Date: 2020/5/25 18:08
 * @Description:抽象的位置(抽象构件)
 */
public interface IPosition {
    /**
     *
     * 功能描述:输出位置名称
     *
     * @param: 
     * @return: 
     * @auther: csh
     * @date: 2020/5/25 18:09
     */
    void printPositionName(String preStr);
}
