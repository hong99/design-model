package com.hong.base.structure_type_model.compositemodel.earth.safety;

/**
 * @Auther: csh
 * @Date: 2020/5/25 18:33
 * @Description:国家
 */
public class Country implements IPosition {

    /**
     *
     * 功能描述:地方名称
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:14
     */
    private String name;

    public Country(String name) {
        this.name = name;
    }

    @Override
    public void printPositionName(String preStr) {
        System.out.println(preStr + "-" + name);
    }
}
