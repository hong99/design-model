package com.hong.base.structure_type_model.compositemodel.earth.lucency;

/**
 * @Auther: csh
 * @Date: 2020/5/26 10:25
 * @Description:透明模式
 */
public class Client {

    public static void main(String[] args) {
        Continent earth = new Continent("地球");
        Continent america = new Continent("美洲");
        Continent asian = new Continent("亚洲");
        Continent africa = new Continent("非洲");

        Country china = new Country("中国");
        asian.addChild(china);
        Country american = new Country("美国");
        america.addChild(american);
        Country cameroon = new Country("喀麦隆");
        africa.addChild(cameroon);

        earth.addChild(asian);
        earth.addChild(america);
        earth.addChild(africa);

        earth.printPositionName("");

    }
}
