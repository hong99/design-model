package com.hong.base.structure_type_model.compositemodel.earth.safety;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 18:13
 * @Description:洲
 */
public class Continent implements IPosition {
    /**
     *
     * 功能描述:各地方
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:14
     */
    private List<IPosition>  childPosition = new ArrayList <IPosition>();
    /**
     *
     * 功能描述:地方名称
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:14
     */
    private String name;

    public Continent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    /**
     *
     * 功能描述:添加
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:16
     */
    public void addChild(IPosition child){
        childPosition.add(child);
    }
    /**
     *
     * 功能描述:删除
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/25 18:16
     */
    public void removeChild(IPosition position){
        childPosition.remove(position);
    }
    /**
     *
     * 功能描述:返回列表
     *
     * @param: 
     * @return: 
     * @auther: csh
     * @date: 2020/5/25 18:16
     */
    public List<IPosition> getChild(){
        return childPosition;
    }



    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void printPositionName(String preStr) {
        //当前的位置
        System.out.println(preStr+"+"+this.name);
        //获取包含的地区
        if(null!=childPosition && childPosition.size()>0){
            preStr+=" ";
            for (IPosition iPosition : childPosition) {
                iPosition.printPositionName(preStr);
            }
        }
    }
}
