package com.hong.base.structure_type_model.compositemodel.earth.lucency;

/**
 * @Auther: csh
 * @Date: 2020/5/26 10:23
 * @Description:国家
 */
public class Country extends IPosition{

    private String name;

    public Country(String name) {
        this.name = name;
    }

    @Override
    public void printPositionName(String preStr) {
        System.out.println(preStr+"-"+name);
    }
}
