package com.hong.base.structure_type_model.compositemodel.simple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/25 16:44
 * @Description:员工
 */
public class Employee implements Serializable {
    /** 姓名*/
    private String name;
    /** 部门*/
    private String dept;
    /** 薪资*/
    private int salary;
    /** 下级员工*/
    private List<Employee> subordinates = new ArrayList <Employee>();

    public Employee(String name, String dept, int salary) {
        this.name = name;
        this.dept = dept;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public List <Employee> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List <Employee> subordinates) {
        this.subordinates = subordinates;
    }

    public void add(Employee e) {
        subordinates.add(e);
    }

    public void remove(Employee e) {
        subordinates.remove(e);
    }


    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary=" + salary +
                ", subordinates=" + subordinates +
                '}';
    }
}
