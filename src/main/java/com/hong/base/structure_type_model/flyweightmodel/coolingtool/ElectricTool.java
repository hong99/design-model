package com.hong.base.structure_type_model.flyweightmodel.coolingtool;

/**
 * @Auther: csh
 * @Date: 2020/6/1 17:36
 * @Description:家电(具体享元角色类)
 */
public class ElectricTool implements ICoolingTool {
    //电器名称
    private String name;
    //档数
    private int number;


    public ElectricTool(String name) {
        this.name = name;
    }
    @Override
    public void useTool() {
        System.out.println("启动了"+name+"开始降温!开了"+number+"档");
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
