package com.hong.base.structure_type_model.flyweightmodel.subject;

/**
 * @Auther: csh
 * @Date: 2020/6/1 16:50
 * @Description:
 */
public class ConcreateFlyweight1 extends Flyweight {
    //接受外部状态
    public ConcreateFlyweight1(String extrinsic) {
        super(extrinsic);
    }

    @Override
    public void operate() {
        //业务逻辑
    }
}
