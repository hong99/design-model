package com.hong.base.structure_type_model.flyweightmodel.simple;

/**
 * @Auther: csh
 * @Date: 2020/6/1 15:59
 * @Description:圆型
 */
public class Circle implements Shape {
    /** 颜色*/
    private String color;
    /** x轴*/
    private int x;
    /** y轴*/
    private int y;
    /** 半径*/
    private int radius;

    public Circle(String color){
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public void draw() {
        System.out.println("圆型: Draw() [Color : " + color
                +", x : " + x +", y :" + y +", radius :" + radius);
    }
}
