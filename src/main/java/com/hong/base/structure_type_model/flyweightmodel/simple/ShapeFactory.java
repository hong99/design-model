package com.hong.base.structure_type_model.flyweightmodel.simple;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/6/1 16:01
 * @Description:型状工厂
 */
public class ShapeFactory {
    private static final HashMap <String, Shape> circleMap = new HashMap <String, Shape>();

    public static Shape getCircle(String color) {
        Circle circle = (Circle) circleMap.get(color);

        if (circle == null) {
            //没有就创建
            circle = new Circle(color);
            circleMap.put(color, circle);
            System.out.println("创建型状的颜色是 : " + color);
        }
        return circle;
    }
}
