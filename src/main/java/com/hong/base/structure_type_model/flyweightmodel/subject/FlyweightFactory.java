package com.hong.base.structure_type_model.flyweightmodel.subject;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/6/1 16:51
 * @Description:
 */
public class FlyweightFactory {
    //定义一个池容器
    private static HashMap<String,Flyweight> pool = new HashMap <String, Flyweight>();
    //享元工厂
    public static Flyweight getFlyweight(String extrinsic){
        //需要返回的对象
        Flyweight flyweight = null;
        //在池中没有该对象
        if(pool.containsKey(extrinsic)){
            flyweight = pool.get(extrinsic);
        }else{
            flyweight = new ConcreateFlyweight1(extrinsic);
            //放置到池中
            pool.put(extrinsic,flyweight);
        }
        return flyweight;
    }
}
