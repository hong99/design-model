package com.hong.base.structure_type_model.flyweightmodel.coolingtool;

/**
 * @Auther: csh
 * @Date: 2020/6/1 17:50
 * @Description:
 * 该例，理解为一个家(共享池)，降温工具(电器)，有就用，没有就添加(创建)，然后放到家(共享池)里面，下次用到直接用就可以了。享元就是家里不用重复装空调，一般一个房间一个就够了，总不可能一直加嘛。将家抽象为一个池子，而各种降温家电为抽象享元类，空调、风扇等具体对象为具体享元类，放置这些家电的地方，墙上、地板等(工厂)，而这些家电，电器名称为(内部状态)，电器档数(外部状态)。
 * 演示
 */
public class Client {
    private static final String[] homeElectricityTool = { "风扇", "冷风扇", "空调", "中央空调" };
    public static void main(String[] args) {
        for(int i=0;i<20;i++){
            ElectricTool tool = (ElectricTool)ElectricToolFactory.getCoolingTool(getRandomElectrici());
            tool.setNumber(getRandomNumber());
            tool.useTool();
        }
        //String中也是使用了享元模式进行储存，内存同样引用同一个地址
        String str1 = "123456";
        String str2 = "123456";
        System.out.println(str1==str2);
        System.out.println(str1.hashCode());
        System.out.println(str2.hashCode());
    }

    private static String getRandomElectrici() {
        return homeElectricityTool[(int)(Math.random()*homeElectricityTool.length)];
    }

    private static int getRandomNumber() {
        return (int)(Math.random()*6 );
    }
}
