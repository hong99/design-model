package com.hong.base.structure_type_model.flyweightmodel.simple;
/**
 *
 * 功能描述:型状接口
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/6/1 15:59
 */
public interface Shape {
   void draw();
}