package com.hong.base.structure_type_model.flyweightmodel.coolingtool;

import java.util.HashMap;

/**
 * @Auther: csh
 * @Date: 2020/6/1 17:38
 * @Description:电器工厂(享元工厂角色类)
 */
public class ElectricToolFactory {
    private static final HashMap<String,ICoolingTool> pool = new HashMap <String, ICoolingTool>();

    public static ICoolingTool getCoolingTool(String electricName){
        ICoolingTool tool = pool.get(electricName);
            //双重校验
            synchronized (ElectricToolFactory.class){
                if(null==tool){
                    synchronized (ElectricToolFactory.class){
                        //没有就添加家电
                        tool = new ElectricTool(electricName);
                        pool.put(electricName,tool);
                        System.out.println("家里新添了家电"+electricName);
                    }
                }
            }
            return tool;
    }
}
