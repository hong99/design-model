package com.hong.base.structure_type_model.flyweightmodel.coolingtool;

/**
 * @Auther: csh
 * @Date: 2020/6/1 17:33
 * @Description:降温工具抽象(抽象享元类)
 */
public  interface ICoolingTool {
    //工具
    void useTool();
}
