package com.hong.base.structure_type_model.bridgemodel.one;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:31
 * @Description:服装公司
 */
public class ClothesCorp extends Corp {

    @Override
    protected void produce() {
        System.out.println("服务公司生产衣服...");
    }

    @Override
    protected void sell() {
        System.out.println("服装公司出售衣服...");
    }

    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("服务公司赚小钱...");
    }
}
