package com.hong.base.structure_type_model.bridgemodel.four;

/**
 * @Auther: csh
 * @Date: 2020/5/15 16:57
 * @Description:
 */
public class HuaWeiMobile extends MobileAbstraction {

    public HuaWeiMobile(ViewFunction viewFunction, String brand) {
        super(viewFunction, brand);
        System.out.println("华为手机");
        viewFunction.view(true);
    }
}
