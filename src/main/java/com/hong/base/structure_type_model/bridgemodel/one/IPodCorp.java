package com.hong.base.structure_type_model.bridgemodel.one;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:36
 * @Description:iPod山塞公司
 */
public class IPodCorp extends Corp {
    @Override
    protected void produce() {
        System.out.println("我生产iPod...");
    }

    @Override
    protected void sell() {
        System.out.println("ipod畅销....");
    }

    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("我赚钱呀...");
    }
}
