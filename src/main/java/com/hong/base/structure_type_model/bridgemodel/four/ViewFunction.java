package com.hong.base.structure_type_model.bridgemodel.four;

/**
 * @Auther: csh
 * @Date: 2020/5/15 16:54
 * @Description:视频抽象
 */
public abstract class ViewFunction
{
    //视频功能
    public abstract void view(Boolean isView);
}
