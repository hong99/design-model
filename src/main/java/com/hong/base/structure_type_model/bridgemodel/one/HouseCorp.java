package com.hong.base.structure_type_model.bridgemodel.one;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:21
 * @Description:房地产公司
 */
public class HouseCorp extends Corp {
    @Override
    protected void produce() {
        System.out.println("房地产公司盖房子...");
    }

    @Override
    protected void sell() {
        System.out.println("房地产公司出食品房子...");
    }

    //公司是干什么的，赚钱的
    @Override
    public void makeMoney(){
        super.makeMoney();
        System.out.println("房地产公司赚大钱了...");
    }
}
