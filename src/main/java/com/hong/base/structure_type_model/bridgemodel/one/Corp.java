package com.hong.base.structure_type_model.bridgemodel.one;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:17
 * @Description:抽象公司
 */
public abstract class Corp {
    /**
     * 如果是公司就应该有生产，不管是软件公司还是制造业公司
     * 每家公司生产的东西都 不一样，所以由实现类来完成
     */
    protected abstract void produce();

    /**
     * 有产品了，那鼐定要销售，不销售公司怎么生存
     */
    protected abstract void sell();
    //公司是干什么的，赚钱的
    public void makeMoney(){
        //每一个公司都是一样，先生产
        this.produce();
        //然后稍售
        this.sell();
    }

}
