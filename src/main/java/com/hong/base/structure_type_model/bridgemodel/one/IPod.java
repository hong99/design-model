package com.hong.base.structure_type_model.bridgemodel.one;

import com.hong.base.structure_type_model.bridgemodel.two.Product;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:44
 * @Description:pod产品
 */
public class IPod extends Product {
    @Override
    public void beProducted() {
        System.out.println("生产出的ipod是这样的...");
    }

    @Override
    public void beSelled() {
        System.out.println("生产出的ipod卖出去了...");
    }
}
