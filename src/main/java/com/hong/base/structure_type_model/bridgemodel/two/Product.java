package com.hong.base.structure_type_model.bridgemodel.two;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:39
 * @Description:抽象产品类
 */
public abstract class Product {

    //歪管是什么产品它总要能被生产出来
    public abstract void beProducted();
    //生产出来的东西，一定要销售出云，否则亏本
    public abstract void beSelled();
}
