package com.hong.base.structure_type_model.bridgemodel.four;

/**
 * @Auther: csh
 * @Date: 2020/5/15 16:51
 * @Description:手机抽象
 */
public class MobileAbstraction {
    protected ViewFunction viewFunction;
    /** 品牌*/
    private String brand;

    public MobileAbstraction(ViewFunction viewFunction, String brand) {
        this.viewFunction = viewFunction;
        this.brand = brand;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
