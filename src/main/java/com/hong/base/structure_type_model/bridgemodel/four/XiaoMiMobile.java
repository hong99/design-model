package com.hong.base.structure_type_model.bridgemodel.four;

/**
 * @Auther: csh
 * @Date: 2020/5/15 16:58
 * @Description:
 */
public class XiaoMiMobile extends MobileAbstraction {
    public XiaoMiMobile(ViewFunction viewFunction, String brand) {
        super(viewFunction, brand);
        System.out.println("小米手机");
        viewFunction.view(false);
    }
}
