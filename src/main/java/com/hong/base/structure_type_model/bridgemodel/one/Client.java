package com.hong.base.structure_type_model.bridgemodel.one;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:32
 * @Description:
 */
public class Client {

    public static void main(String[] args) {
        System.out.println("-------------房地产公司是这样运行的----------");
        //先找到我的公司
        HouseCorp houseCorp = new HouseCorp();
        //看我怎么挣钱
        houseCorp.makeMoney();
        System.out.println("\n");
        System.out.println("----------服装公司是这样运行的---------------");
        ClothesCorp clothesCorp = new ClothesCorp();
        clothesCorp.makeMoney();
        System.out.println("----------------山塞公司是按这样运行的-------------------------");
        IPodCorp podCorp = new IPodCorp();
        podCorp.makeMoney();
    }
}
