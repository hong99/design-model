package com.hong.base.structure_type_model.bridgemodel.two;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:41
 * @Description:豆腐渣就是豆腐呗，好歹也是房子
 */
public class House extends Product {
    @Override
    public void beProducted() {
        System.out.println("生产出来的房子是这样的...");
    }

    @Override
    public void beSelled() {
        System.out.println("生产出来的房子卖出去了...");
    }


}
