package com.hong.base.structure_type_model.bridgemodel.two;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:50
 * @Description:山塞公司
 */
public class ShanZhaiCorp extends Corp {

    public ShanZhaiCorp(Product product) {
        super(product);
    }

    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("我赚钱哎...");
    }
}
