package com.hong.base.structure_type_model.bridgemodel.four;

/**
 * @Auther: csh
 * @Date: 2020/5/15 17:01
 * @Description:功能实现
 */
public class SupportViewFunction extends ViewFunction {


    @Override
    public void view(Boolean isView) {
        if(isView){
            System.out.println("支持视频！");
        }else{
            System.out.println("不支持视频!");
        }
    }
}
