package com.hong.base.structure_type_model.bridgemodel.two;

/**
 * @Auther: csh
 * @Date: 2020/5/15 11:48
 * @Description:
 */
public class HouseCorp extends Corp {
    //定义传递一个House产品进来
    public HouseCorp(Product product) {
        super(product);
    }

    //房地产公司High了、赚钱、计算利润
    @Override
    public void makeMoney(){
        super.makeMoney();
        System.out.println("房地产公司赚大钱了...");
    }
}
