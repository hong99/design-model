package com.hong.base.structure_type_model.bridgemodel.two;

/**
 * @Auther: csh
 * @Date: 2020/5/15 14:49
 * @Description:
 */
public class Clothes extends Product {
    @Override
    public void beProducted() {
        System.out.println("生产出的衣服是这样的...");
    }

    @Override
    public void beSelled() {
        System.out.println("生产出的衣服卖出去了...");
    }


}
