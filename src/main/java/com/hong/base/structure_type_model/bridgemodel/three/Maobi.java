package com.hong.base.structure_type_model.bridgemodel.three;

public abstract class Maobi {

    protected MyColor color;
    
    public void setColor(MyColor color){
        this.color=color;
    }
    
    public abstract void paint();
}