package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 16:49
 * @Description:小米9
 */
public class NightMobileImpl implements NightMobile {

    private static NightMobileImpl nightMobile = new NightMobileImpl();

    private NightMobileImpl() {
    }
    //单列 饿汉
    public static final NightMobileImpl getInstance(){
        return nightMobile;
    }

    @Override
    public void showModel() {
        System.out.println("小米9");
    }
}
