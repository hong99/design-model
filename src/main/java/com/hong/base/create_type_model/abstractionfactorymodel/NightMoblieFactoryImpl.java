package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 17:44
 * @Description:小米9工厂实现
 */
public class NightMoblieFactoryImpl implements IMobileFactory {
    //单例
    private static final NightMoblieFactoryImpl moblieFacotry = new NightMoblieFactoryImpl();

    private NightMoblieFactoryImpl(){

    }

    public static NightMoblieFactoryImpl getInstance(){
        return moblieFacotry;
    }

    @Override
    public AbstractMoblie crateEightMobile() {
        return NightMobileImpl.getInstance();
    }
}
