package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 16:50
 * @Description:小米10实现
 */
public class TenMobileImpl implements TenMobile {

    private static TenMobileImpl tenMobile = new TenMobileImpl();

    private TenMobileImpl(){

    }

    public static final TenMobileImpl getInstance(){
        return tenMobile;
    }

    @Override
    public void showModel() {
        System.out.println("小米10");
    }
}
