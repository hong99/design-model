package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 17:02
 * @Description:抽象手机接口
 */
public interface AbstractMoblie {

    void showModel();
}
