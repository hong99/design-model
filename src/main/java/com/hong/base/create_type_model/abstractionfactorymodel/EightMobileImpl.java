package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 16:49
 * @Description:小米8实现
 */
public class EightMobileImpl implements EightMobile {
    //单列
    private EightMobileImpl(){

    }
    //单列 饿汉
    private static EightMobileImpl eightMobile = new EightMobileImpl();

    public static final EightMobileImpl getInstance(){
        return eightMobile;
    }

    @Override
    public void showModel() {
        System.out.println("小米8");
    }
}
