package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 17:44
 * @Description:小米10工厂实现
 */
public class TenMoblieFactoryImpl implements IMobileFactory {

    //单例
    private static final TenMoblieFactoryImpl moblieFacotry = new TenMoblieFactoryImpl();

    private TenMoblieFactoryImpl(){

    }

    public static TenMoblieFactoryImpl getInstance(){
        return moblieFacotry;
    }

    @Override
    public AbstractMoblie crateEightMobile() {
        return TenMobileImpl.getInstance();
    }
}
