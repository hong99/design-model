package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 17:44
 * @Description:小米8工厂实现
 */
public class EightMoblieFactoryImpl implements IMobileFactory {


    //单例
    private static final EightMoblieFactoryImpl moblieFacotry = new EightMoblieFactoryImpl();

    private EightMoblieFactoryImpl(){

    }

    public static EightMoblieFactoryImpl getInstance(){
        return moblieFacotry;
    }

    @Override
    public AbstractMoblie crateEightMobile() {
        return EightMobileImpl.getInstance();
    }
}
