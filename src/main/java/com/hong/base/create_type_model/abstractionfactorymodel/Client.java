package com.hong.base.create_type_model.abstractionfactorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 16:56
 * @Description:通过不同的工厂类创建
 */
public class Client {
    public static void main(String[] args) {
        //小米8
        IMobileFactory eightFactory =EightMoblieFactoryImpl.getInstance();
        AbstractMoblie eightMobile = eightFactory.crateEightMobile();
        eightMobile.showModel();
        //小米9
        IMobileFactory mobileFactory =NightMoblieFactoryImpl.getInstance();
        AbstractMoblie nightMobile = mobileFactory.crateEightMobile();
        nightMobile.showModel();
        //小米10
        IMobileFactory tenFactory =NightMoblieFactoryImpl.getInstance();
        AbstractMoblie tenMobile = tenFactory.crateEightMobile();
        tenMobile.showModel();

    }
}
