package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 11:49
 * @Description:戴尔笔记本(builder角色)
 */
public class DellComputerBuilder extends Builder {

    private Computer computer = new Computer();

    @Override
    public void buildBrand() {
        computer.setBrand("戴尔笔记本");
    }

    @Override
    public void buildCpu() {
        computer.setCpu("16核");
    }

    @Override
    public void buildMainBoard() {
        computer.setMainBorad("1万块主板");
    }


    @Override
    public void buildDisplay() {
        computer.setDisplayer("戴尔显示器");
    }

    @Override
    public Computer getComputer() {
        return computer;
    }

}
