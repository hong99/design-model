package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 14:11
 * @Description:联想建造者
 */
public class LenovoComputerBuilder extends Builder {

    private Computer computer = new Computer();

    @Override
    public void buildBrand() {
        computer.setBrand("联想电脑");
    }

    @Override
    public void buildCpu() {
        computer.setCpu("低配版");
    }

    @Override
    public void buildMainBoard() {
        computer.setMainBorad("低配主板");
    }


    @Override
    public void buildDisplay() {
        computer.setDisplayer("16寸显示器");
    }

    @Override
    public Computer getComputer() {
        return computer;
    }
}
