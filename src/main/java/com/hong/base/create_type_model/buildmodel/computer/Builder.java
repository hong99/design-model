package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 11:13
 * @Description:抽象建造者
 */
public abstract class Builder {
    //吕牌
    public abstract void buildBrand();
    //cpu
    public abstract void buildCpu();
    //主版
    public abstract void buildMainBoard();
    //显示器
    public abstract void buildDisplay();
    //获取电脑
    public abstract Computer getComputer();

}
