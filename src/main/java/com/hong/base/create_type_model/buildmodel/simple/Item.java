package com.hong.base.create_type_model.buildmodel.simple;
//抽象食品接口
public interface Item {
   //名称
   public String name();
   //包装
   public Packing packing();
   //价格
   public float price();    
}