package com.hong.base.create_type_model.buildmodel.simple;

public class Bottle implements Packing {
 
   @Override
   public String pack() {
      return "Bottle";
   }
}