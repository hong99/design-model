package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 14:13
 * @Description:组装类（指挥者）
 * 该类是指挥拼专的，就像以前的监工
 */
public class ComputerDirector {
    public Computer construct(Builder builder){
        //构建产品
        Computer computer;
        builder.buildBrand();
        builder.buildCpu();
        builder.buildDisplay();
        builder.buildMainBoard();
        computer = builder.getComputer();
        return computer;
    }
}
