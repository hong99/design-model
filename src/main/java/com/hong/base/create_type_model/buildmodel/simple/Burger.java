package com.hong.base.create_type_model.buildmodel.simple;
//汉堡抽象类
public abstract class Burger implements Item {
 
   @Override
   public Packing packing() {
      return new Wrapper();
   }
 
   @Override
   public abstract float price();
}