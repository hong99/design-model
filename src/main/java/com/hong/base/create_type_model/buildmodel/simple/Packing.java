package com.hong.base.create_type_model.buildmodel.simple;

public interface Packing {
   public String pack();
}