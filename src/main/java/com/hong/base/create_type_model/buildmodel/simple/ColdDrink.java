package com.hong.base.create_type_model.buildmodel.simple;
//饮料抽象类
public abstract class ColdDrink implements Item {
 
    @Override
    public Packing packing() {
       return new Bottle();
    }
 
    @Override
    public abstract float price();
}