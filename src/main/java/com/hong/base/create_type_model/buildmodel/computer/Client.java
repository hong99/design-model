package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 14:15
 * @Description:建造者模式测试
 */
public class Client {
    public static void main(String[] args) {
        ComputerDirector director = new ComputerDirector();
        Computer construct = director.construct(new LenovoComputerBuilder());
        System.out.println(construct);
        System.out.println("-----------------------------------------------");
        Computer construct1 = director.construct(new DellComputerBuilder());
        System.out.println(construct1);
    }
}
