package com.hong.base.create_type_model.buildmodel.computer;

/**
 * @Auther: csh
 * @Date: 2020/5/18 11:17
 * @Description:抽象电脑（抽象产品）
 */
public class Computer {
    //cpu
    private String cpu;
    //品牌
    private String brand;
    //主板
    private String mainBorad;
    //显示器
    private String displayer;
    //价格
    private String money;

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMainBorad() {
        return mainBorad;
    }

    public void setMainBorad(String mainBorad) {
        this.mainBorad = mainBorad;
    }

    public String getDisplayer() {
        return displayer;
    }

    public void setDisplayer(String displayer) {
        this.displayer = displayer;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "cpu='" + cpu + '\'' +
                ", brand='" + brand + '\'' +
                ", mainBorad='" + mainBorad + '\'' +
                ", displayer='" + displayer + '\'' +
                ", money='" + money + '\'' +
                '}';
    }
}
