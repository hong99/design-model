package com.hong.base.create_type_model.prototypemodel;

/**
 * @Auther: csh
 * @Date: 2020/5/18 16:07
 * @Description:原型测试
 * 浅克隆：被克隆对象的所有变量都含有与原来的对象相同的值，而它所有的对其他对象的引用都仍然指向原来的对象。换一种说法就是浅克隆仅仅克隆所考虑的对象，而不克隆它所引用的对象。
 *
 *  深克隆：被克隆对象的所有变量都含有与原来的对象相同的值，但它所有的对其他对象的引用不再是原有的，而这是指向被复制过的新对象。换言之，深复制把要复制的对象的所有引用的对象都复制了一遍，这种叫做间接复制。
 *
 */
public class Client {
    public static void main(String[] args) {

        try {
            //拷贝
            Virus copyVirus = new Virus();
            System.out.println(copyVirus);
            Virus clone = copyVirus.clone();
            System.out.println(clone);

            //浅拷贝
            ShallowVirus virus = new ShallowVirus();
            virus.addName("新冠状1");
            System.out.println("原病毒"+virus);
            ShallowVirus newVirus = virus.clone();
            newVirus.addName("新冠状2");
            System.out.println("新病毒"+newVirus);
            virus.printNames();
            System.out.println("----------");
            newVirus.printNames();
            //深拷贝
            System.out.println("----------");
            DeepVirus deepVirus = new DeepVirus();
            deepVirus.addName("SAAS1");
            DeepVirus deepVirus2 = deepVirus.clone();
            deepVirus2.addName("SAAS2");
            deepVirus.printNames();
            System.out.println("---------------");
            deepVirus2.printNames();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
