package com.hong.base.create_type_model.prototypemodel;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/18 16:05
 * @Description:浅拷贝 病毒
 */
public class ShallowVirus implements Cloneable {

    private List<String> list = new ArrayList <String>();


    public List <String> getList() {
        return list;
    }

    public void addName(String name){
        list.add(name);
    }

    public void printNames(){
        for (String name : list) {
            System.out.println(name);
        }
    }

    public void setList(List <String> list) {
        this.list = list;
    }

    @Override
    protected ShallowVirus clone() throws CloneNotSupportedException {
        return (ShallowVirus)super.clone();
    }
}
