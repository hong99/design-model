package com.hong.base.create_type_model.prototypemodel;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: csh
 * @Date: 2020/5/18 16:05
 * @Description:深拷贝 病毒
 *
 *
 */
public class DeepVirus implements Cloneable {

    private List<String> list = new ArrayList <String>();


    public List <String> getList() {
        return list;
    }

    public void addName(String name){
        list.add(name);
    }

    public void printNames(){
        for (String name : list) {
            System.out.println(name);
        }
    }

    public void setList(List <String> list) {
        this.list = list;
    }

    @Override
    protected DeepVirus clone() throws CloneNotSupportedException {
        try {
            DeepVirus deepVirus =   (DeepVirus)super.clone();
            List<String> newList = new ArrayList <String>();
            for (String val : this.getList()) {
                newList.add(val);
            }
            //再把这个list复制到对象中
            deepVirus.setList(newList);
            return deepVirus;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }
}
