package com.hong.base.create_type_model.factorymodel;
/**
 *
 * 功能描述: 长方型
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/16 17:48
 */
public class Rectangle implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Rectangle::draw() method.");
   }
}