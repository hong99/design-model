package com.hong.base.create_type_model.factorymodel;
/**
 *
 * 功能描述:圆型
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/16 17:47
 */
public class Circle implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Circle::draw() method.");
   }
}