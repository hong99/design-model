package com.hong.base.create_type_model.factorymodel;
/**
 *
 * 功能描述:生成型状工厂
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/16 17:49
 */
public class ShapeFactory {
    
   //使用 getShape 方法获取形状类型的对象
   public Shape getShape(String shapeType){
      if(shapeType == null){
         return null;
      }        
      if(shapeType.equalsIgnoreCase("CIRCLE")){
         return new Circle();
      } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
         return new Rectangle();
      } else if(shapeType.equalsIgnoreCase("SQUARE")){
         return new Square();
      }
      return null;
   }
}