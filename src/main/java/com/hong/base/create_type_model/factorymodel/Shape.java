package com.hong.base.create_type_model.factorymodel;

/**
 * @Auther: csh
 * @Date: 2020/5/16 17:26
 * @Description:接口类
 */
public interface Shape {
    void draw();
}
