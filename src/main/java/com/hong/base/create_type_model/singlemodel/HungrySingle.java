package com.hong.base.create_type_model.singlemodel;
/**
 *
 * 功能描述:单例(俄汉) 线程安全
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/17 10:49
 */
public class HungrySingle {
    //创建 HungrySingle 的一个对象    、
    private static HungrySingle instance = new HungrySingle();

    //让构造函数为 private，这样该类就不会被实例化
    private HungrySingle() {
    }      //获取唯一可用的对象

    public static HungrySingle getInstance() {
        return instance;
    }

    public void showMessage() {
        System.out.println("Hello World!");
    }
}