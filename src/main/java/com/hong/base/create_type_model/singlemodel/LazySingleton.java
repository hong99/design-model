package com.hong.base.create_type_model.singlemodel;
/**
 *
 * 功能描述:懒汉（线程不安全）
 *
 * @param:
 * @return:
 * @auther: csh
 * @date: 2020/5/17 10:55
 */
public class LazySingleton {
    private static LazySingleton instance;
    private LazySingleton(){}
    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}