package com.hong.base.create_type_model.singlemodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 11:33
 * @Description:双检锁/双重校验锁（DCL，即 double-checked locking）
 * 性能较高
 */
public class DoubleCheckLazySingleSafery {
    private static DoubleCheckLazySingleSafery instance;
    private DoubleCheckLazySingleSafery(){

    }

    public static DoubleCheckLazySingleSafery getInstance(){
        if(instance == null){
            synchronized (DoubleCheckLazySingleSafery.class){
                if(instance == null){
                    instance = new DoubleCheckLazySingleSafery();
                }
            }
        }
        return instance;
    }
}
