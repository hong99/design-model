package com.hong.base.create_type_model.singlemodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 11:27
 * @Description:懒汉 （线程安全） 缺点：性能较低
 */
public class LazySingleSafery {
    private static LazySingleSafery lazySingleSafery;

    private LazySingleSafery(){

    }
    /**
     *
     * 功能描述:使用了同步类锁导致性能低下
     *
     * @param:
     * @return:
     * @auther: csh
     * @date: 2020/5/17 11:32
     */
    public static synchronized LazySingleSafery getInstance(){
        if(lazySingleSafery == null){
            lazySingleSafery = new LazySingleSafery();
        }
        return lazySingleSafery;
    }
}
