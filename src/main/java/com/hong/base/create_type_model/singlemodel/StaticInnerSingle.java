package com.hong.base.create_type_model.singlemodel;

/**
 * @Auther: csh
 * @Date: 2020/5/17 11:39
 * @Description:登记式/静态内部类  线程安全
 */
public class StaticInnerSingle {
    private static class SingleHolder{
        private static final StaticInnerSingle INSTANCE = new StaticInnerSingle();
    }
    private StaticInnerSingle(){

    }

    public static final StaticInnerSingle getInstance(){
        return SingleHolder.INSTANCE;
    }
}
