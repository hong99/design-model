package com.hong.base.create_type_model.singlemodel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Auther: csh
 * @Date: 2020/5/17 10:43
 * @Description:实现
 */
public class SingletonPatternDemo {
    public static void main(String[] args) {
        //不合法的构造函数
        //编译时错误：构造函数 HungrySingle() 是不可见的
        //HungrySingle object = new HungrySingle();

        //获取唯一可用的对象 饿汉
        ExecutorService lazyThreadPool = Executors.newFixedThreadPool(10);
        for (int i =0;i<10;i++){
            lazyThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("饿汉（安全）"+ HungrySingle.getInstance());
                }
            });
        }
        //懒汉 线程不安全
        ExecutorService hungryThreadPool = Executors.newFixedThreadPool(10);
        for (int i =0;i<10;i++){
            hungryThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("懒汉（不安全）"+LazySingleton.getInstance());
                }
            });
        }

        //懒汉 线程安全 性能低
        ExecutorService hungrySaferyThreadPool = Executors.newFixedThreadPool(10);
        for (int i =0;i<10;i++){
            hungrySaferyThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("懒汉（安全）"+LazySingleSafery.getInstance());
                }
            });
        }

        //懒汉 线程安全 性能高
        ExecutorService DoubleCheckThreadPool = Executors.newFixedThreadPool(10);
        for (int i =0;i<10;i++){
            DoubleCheckThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("懒汉双重检查（安全）"+DoubleCheckLazySingleSafery.getInstance());
                }
            });
        }


        //懒汉 线程安全 性能高
        ExecutorService staticInnerSingleThreadPool = Executors.newFixedThreadPool(10);
        for (int i =0;i<10;i++){
            staticInnerSingleThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("登记式/静态内部类（安全）"+StaticInnerSingle.getInstance());
                }
            });
        }


    }
}


